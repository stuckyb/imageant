
# Building the ImageAnt executable for MacOS

To build the executable, you will need to have PyInstaller and ensure that all of ImageAnt's required libraries are available in the build environment.  Then, run
```
pyinstaller imageant.spec
```

That should create an updated `ImageAnt.app` in the `dist` folder.


## Creating the distributable disk image

The app bundle should be packaged in a disk image for easy distribution.  To do that, first run
```
hdiutil create -srcfolder ImageAnt.app/ -fs HFS+ -volname ImageAnt imageant-rw.dmg
```

That will create the disk image, which should then be converted to a read-only disk image:
```
hdiutil convert imageant-rw.dmg -format UDZO -o imageant.dmg 
```

To check the results, run:
```
hdiutil imageinfo imageant.dmg
```

