
# Building the ImageAnt executable for Windows

To build the executable, you will need to have PyInstaller and ensure that all of ImageAnt's required libraries are available in the build environment.  Then, run
```
pyinstaller imageant.spec
```

That should create an updated `imageant.exe` in the `dist` folder.


## Creating a new spec file

The pyinstaller spec file, `imageant.spec` can be created with this command:
```
pyinstaller -F -n imageant -w  ..\..\src\imageant_main.py
```

After creating a new spec file, edit the spec file and change the generated value of the `pathex` parameter of the `Analysis` constructor to `None`.

