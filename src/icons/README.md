
# Icon theme for ImageAnt

To ensure cross-platform icon availability, all ImageAnt UI icons are included with the distribution.  Most of the icons were taken from the [elementary-xfce icon theme](https://github.com/shimmerproject/elementary-xfce) for the [XFCE Desktop Environment](https://www.xfce.org/).  One icon (`document-open-imagefolder` (originally `emblem-photos`)) was taken from the [GNOME icon theme](https://ftp.gnome.org/pub/GNOME/sources/gnome-icon-theme/) for the [GNOME Desktop Environment](https://www.gnome.org/).  The icons `annotation-session-image-new`, `annotation-session-audio-new`, `annotation-session-restore`, and `edit-undo-image` were created for ImageAnt using `document-new`, `audio-volume-high`, `view-refresh`, and `edit-undo` from the elementary-xfce icon theme and `emblem-photos` from the GNOME icon theme.

The icon folder organization and metadata follow [freedesktop.org's icon theme specification](https://www.freedesktop.org/wiki/Specifications/icon-theme-spec/).


## Building the icon theme

To build the icon theme, run `make` in the `src/icons` folder.  Note that you will need the GTK+ development libraries, `optipng`, and `pyrcc5`.  After a successful build, the updated icon resource file will be `build/ia_icons.py`.

If you modify any of the icons, add them to git's staging area, then run `pre-commit.sh` before commiting the changes to the repository (you might have to `add` them again after running `pre-commit.sh`).


## License

These icons are free artwork; you can redistribute them and/or modify them under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2, or (at your option) any later version.

These icons distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with these icons; if not, see <http://www.gnu.org/licenses/>.

GNOME icons, Humanity icons, and elementary icons are all licensed under the GPL.

Icons based on GNOME and other GNOME projects are licensed GPL.  You can visit the GNOME website here: http://www.gnome.org/

Icons based on Tango sources or taken from the Tango project are public domain.  You can visit the Tango project website here:  http://tango.freedesktop.org/Tango_Desktop_Project

Icons based on Humanity sources or taken from the elementary project are licensed GPL.  You can visit the Humanity website here: http://launchpad.net/humanity


## Credits for the elementary-xfce icon theme (for the version reused here)

The elementary icons are designed and developed by Daniel Foré <daniel@elementaryos.org> at https://github.com/elementary/icons.

The elementary-xfce variant - supporting all Desktop Environments, but developed for Xfce - is currently maintained by Simon Steinbeiß <simon@xfce.org>.

Contributors:
  * Sebastian Porta <sebastianporta@gmail.com>
  * Sergey "Shnatsel" Davidoff <shnatsel@gmail.com>
  * Oliver Scholtz <scholli_tz@yahoo.de>
  * Dennis Fisher
  * Simon Steinbeiß <simon.steinbeiss@elfenbeinturm.at>
  * Pasi Lallinaho <pasi@shimmerproject.org>
  * Sean Davis <smd.seandavis@gmail.com>
  * Sérgio Benjamim <sergiobenrocha2@gmail.com>
  * spg76


## Credits for the GNOME icon theme (for the version reused here)

  * Ulisse Perusin <uli.peru@gmail.com>
  * Riccardo Buzzotta <raozuzu@yahoo.it>
  * Josef Vybíral <cornelius@vybiral.info>
  * Hylke Bons <h.bons@gmail.com>
  * Ricardo González <rick@jinlabs.com>
  * Lapo Calamandrei <calamandrei@gmail.com>
  * Rodney Dawes <dobey@novell.com>
  * Luca Ferretti <elle.uca@libero.it>
  * Tuomas Kuosmanen <tigert@gimp.org>
  * Andreas Nilsson <nisses.mail@home.se>
  * Jakub Steiner <jimmac@novell.com>

Some external 3D Assets used:
Geraldo Cockerhan - http://www.blendswap.com/blends/view/40495 CCBYSA

