#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# A simple script to make it easier to copy icons between themes or contexts.
#

import sys
import os.path
import shutil
from argparse import ArgumentParser


argp = ArgumentParser(
    description='Copies icon files between themes or contexts.  SOURCE_FOLDER '
    'and DEST_FOLDER should contain icon files organized into subfolders '
    'according to target pixel dimensions (16/, 22/, 24/, etc.), and '
    'cp_icons.py will copy the source icon files from each size subfolder to '
    'the maching size subfolder in the destination folder.'
)
argp.add_argument(
    '--source_folder', type=str, required=True,
    help='A folder containing icon files organized into subfolders according '
    'to target pixel dimensions ("16/", "22/", "24/", etc.).'
)
argp.add_argument(
    '--dest_folder', type=str, required=True,
    help='A folder containing icon files organized into subfolders according '
    'to target pixel dimensions ("16/", "22/", "24/", etc.).'
)
argp.add_argument(
    '--source_fname', type=str, required=True,
    help='The source icon file name.'
)
argp.add_argument(
    '--dest_fname', type=str, required=False, default='',
    help='The destination icon file name (default: SOURCE_FNAME).'
)

args = argp.parse_args()

dest_fname = args.dest_fname
if dest_fname == '':
    dest_fname = args.source_fname

sizes = [16, 22, 24, 32, 48, 64, 96, 128]

for size in sizes:
    srcpath = os.path.join(args.source_folder, str(size), args.source_fname)
    destpath = os.path.join(args.dest_folder, str(size), dest_fname)

    if os.path.isfile(srcpath):
        print('Copying {0} to {1}...'.format(srcpath, destpath))
        try:
            shutil.copyfile(srcpath, destpath)
        except FileNotFoundError as err:
            print(err)

