#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Generates a Qt resource file for ImageAnt's icon theme.
#
# Usage: make_qrc.py ICON_DIR [BASE_DIR]
#
# ICON_DIR is the root of the directory to catalog in the qrc file; BASE_DIR is
# a directory from which to generate relative paths (default: ".").
#

import sys
import os
import os.path


def int_str(keyval):
    """
    Supports sorting icon size directories using numerical order.
    """
    try:
        int(keyval)
        if len(keyval) < 3:
            return '0' + keyval
    except:
        pass

    return keyval


if len(sys.argv) < 2 or len(sys.argv) > 3:
    print('ERROR: No icon directory provided.')
    sys.exit(1)

icon_dir = sys.argv[1]
if len(sys.argv) == 2:
    base_dir = '.'
else:
    base_dir = sys.argv[2]

print('<RCC>\n  <qresource>')

for dirpath, dirnames, fnames in os.walk(icon_dir, topdown=True):
    # With a top-down traversal, dirnames can be modified in place.
    dirnames.sort(key=int_str)

    for fname in fnames:
        fpath = os.path.join(dirpath, fname)
        fpath = os.path.relpath(fpath, base_dir)
        print('    <file>{0}</file>'.format(fpath))

    if len(fnames) != 0:
        print()

print('  </qresource>\n</RCC>')

