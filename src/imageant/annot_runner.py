# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os.path
from .observable_event import ObservableEvent
from .task_script import TaskResponse, TaskScriptParser, TaskScriptInterpreter
from .mediafile_annot_manager import MediaFileAnnotationsManager
from .task_undo_stack import TaskUndoStack


def _annotationStateChange(method):
    """
    A decorator that adds automatic monitoring of observable annotation state
    change events to AnnotationsRunner class methods that can change the
    annotation state.  It should detect state changes even if exceptions occur
    in the wrapped method.
    """
    def wrapper(self, *args, **kwargs):
        # Get annotations state.
        prev_task = self.getActiveTask()
        prev_mobj = self.getActiveMediaObject()
        prev_accept = self._accept_annots
        prev_save_state = self._save_required
        prev_annot_complete = self.getAnnotationsComplete()

        # Get undo state.
        prev_undoable = len(self._undo_stack) > 0
        prev_undoable_media_obj = self.hasUndoablePrevMediaObj()

        try:
            return method(self, *args, **kwargs)
        finally:
            if prev_task != self.getActiveTask():
                self.annotationTaskChange(self.getActiveTask())

            if prev_mobj != self.getActiveMediaObject():
                self.mediaObjectChange(self.getActiveMediaObject())

            if prev_accept != self._accept_annots:
                self.annotationStateChange(self._accept_annots)

            if prev_save_state != self._save_required:
                self.saveStateChange(self._save_required)

            undoable = len(self._undo_stack) > 0
            if prev_undoable != undoable:
                self.taskUndoStateChange(undoable)

            undoable_media_obj = self.hasUndoablePrevMediaObj()
            if prev_undoable_media_obj != undoable_media_obj:
                self.mediaObjUndoStateChange(undoable_media_obj)

            annot_complete = self.getAnnotationsComplete()
            if annot_complete and (prev_annot_complete != annot_complete):
                self.annotationsCompleted()

    return wrapper


class AnnotationsRunner:
    """
    The central class that orchestrates annotation tasks.  It serves as a
    mediator among the various low-level objects involved (script, script
    interpreter, annotations manager, etc.), implements a command history for
    undo operations, and provides a high-level interface to the low-level
    annotations system.
    """
    def __init__(self):
        # The active script file and script.
        self._script_file = None
        self._script = None

        # Script parser and interpreter.
        self._parser = TaskScriptParser()
        self._script_interp = None

        # The annotations manager.
        self._am = None

        # Undo history.
        self._undo_stack = TaskUndoStack()

        # A flag that indicates whether new annotations are currently accepted.
        self._accept_annots = False

        # A flag that indicates whether the in-memory annotations are in sync
        # with an output file.
        self._save_required = False

        # Define all externally observable events.

        # An event that fires whenever the task undo state changes (i.e., when
        # the undo history changes from empty to non-empty and vice versa).
        # The observer should be a callable that accepts a single argument,
        # which will be True if undo is available or False otherwise.
        self.taskUndoStateChange = ObservableEvent()
            
        # An event that fires whenever the media object undo state changes
        # (e.g., when it becomes possible to undo annotations on a previous
        # media object).  The observer should be a callable that accepts a
        # single argument, which will be True if previous media object undo is
        # available or False otherwise.
        self.mediaObjUndoStateChange = ObservableEvent()

        # An event that fires whenever the annotation task changes.  The
        # observer should be a callable that accepts a single argument, which
        # will be an instance of AnnotationTask or None.
        self.annotationTaskChange = ObservableEvent()

        # An event that fires whenever the media object changes.  The observer
        # should be a callable that accepts a single argument, which will be
        # the ID of the media object or None.
        self.mediaObjectChange = ObservableEvent()

        # An event that fires when the annotation state changes.  This state
        # indicates whether or not the AnnotationsRunner is accepting
        # annotations.  Note that this state is not strictly coupled with the
        # annotation task state.  E.g., an annotation task can be loaded with
        # both a media object and a question, but if no output files have been
        # set the AnnotationsRunner cannot accept annotations.  The observer
        # should be a callable that accepts a single argument, which will be
        # True if annotations are accepted or False otherwise.
        self.annotationStateChange = ObservableEvent()

        # An event that fires when annotations are completed for all media
        # objects.  The observer should be a callable that does not take any
        # arguments.
        self.annotationsCompleted = ObservableEvent()

        # An event that fires when the annotation save state changes.  The
        # observer should be a callable that accepts a single argument, which
        # will be True if the in-memory annotations are out of sync with the
        # annotations on disk.
        self.saveStateChange = ObservableEvent()

    def __del__(self):
        if self._am is not None:
            self._am.close()

    @_annotationStateChange
    def reset(self):
        """
        Resets the annotation runner to an empty state (i.e., no script or
        image folder is loaded).
        """
        self._reset()

    def _reset(self):
        """
        Internal reset method that does not trigger state change events.
        """
        self._undo_stack.clear()

        self._script_file = None
        self._script = None
        self._script_interp = None

        if self._am is not None:
            self._am.close()
        self._am = None

        self._accept_annots = False

        self._save_required = False

    def _loadAnnotationScript(self, filepath):
        try:
            self._script = self._parser.parseFromFile(filepath)
        except Exception as err:
            self._script = None
            raise

        if len(self._script) == 0:
            self._script = None
            raise Exception(
                'The task script "{0}" does not have any annotation tasks '
                'defined.'.format(filepath)
            )

        self._script_file = filepath
        self._script_interp = TaskScriptInterpreter(self._script)

    @_annotationStateChange
    def initAnnotations(
        self, script_path, media_folder, output_fname, log_fname, restore,
        file_exts=None
    ):
        """
        Initializes the AnnotationsRunner with a media folder, annotations
        script, and output files.  This method treats these as a single, atomic
        operation that only triggers state change events once, after all
        initialization is complete.

        script_path: The path of an annotation script.
        media_folder: The path of a folder containing media files.
        output_fname: The main output CSV file path.
        log_fname: Path to use for the log file.
        restore: If True, restore state from an extant log file.
        file_exts (list[str]): A list of file extensions by which to filter the
            media folder contents.  Extension matching is not case-sensitive.
            If None, all files in the folder will be used.
        """
        self._reset()

        self._loadAnnotationScript(script_path)
        self._am = MediaFileAnnotationsManager(
            media_folder, output_fname, log_fname, restore, file_exts
        )

        if self.getActiveMediaObject() is None:
            # There are no images to annotate after restoring the logfile.
            self._script_interp.stop()

        if (
            self.getActiveTask() is not None and
            self.getActiveMediaObject() is not None
        ):
            self._accept_annots = True

        if restore:
            self._save_required = True

    def hasUndoablePrevMediaObj(self):
        """
        Returns True if there is an undoable media object on the undo stack
        that is not the current media object.
        """
        if self._am is None or len(self._undo_stack) == 0:
            return False
        else:
            return self._undo_stack.hasRewindableLastObject(
                self._am.getActiveMediaFile()
            )

    def getScriptFile(self):
        return self._script_file

    def getOutputFile(self):
        if self._am is not None:
            return self._am.getOutputFile()
        else:
            return None

    def getAnnotationsComplete(self):
        """
        Returns True if all media objects have been annotated.
        """
        # All AnnotationsManager objects are guaranteed to have at least one
        # media object, so the following is a sufficient test for detecting
        # when all media objects are annotated.
        return (
            (self._am is not None) and (self._am.getActiveMediaFile() is None)
        )

    def getActiveMediaObject(self):
        if self._am is None:
            return None
        else:
            mobj_id = self._am.getActiveMediaFile()

            if mobj_id is not None:
                return self._am.getMediaFilePath(mobj_id)
            else:
                return None

    def getActiveTask(self):
        if self._script_interp is None:
            return None
        else:
            return self._script_interp.getActiveTask()

    def getMediaObjCount(self):
        if self._am is None:
            raise RuntimeError('No media objects are loaded.')
        else:
            return self._am.getMediaFileCount()

    def getAnnotatedMediaObjCount(self):
        if self._am is None:
            raise RuntimeError('No media objects are loaded.')
        else:
            return self._am.getAnnotatedMediaFileCount()

    def updateMediaObjTime(self, time):
        """
        Logs a time interval for the current media object without modifying
        annotation state or logging any other action.
        """
        self._am.updateMediaFileTime(time)

    @_annotationStateChange
    def setResponse(self, response_id, time):
        """
        Sets the response to an annotation task.

        response_id: The ID of the response.
        time: The time interval associated with the response.
        """
        if not(self._accept_annots):
            raise RuntimeError('Invalid attempt to set a new annotation.')

        self._undo_stack.push(
            self._am.getActiveMediaFile(), self._script_interp.getStateMemento()
        )

        if response_id == TaskResponse.RESPONSE_SKIP:
            self._am.skipActiveMediaFile(time)

            self._script_interp.run()
        else:
            self._script_interp.setResponse(response_id)

            if self._script_interp.getActiveTask() is None:
                # Annotations are complete for the active image.
                self._am.setAnnotations(
                    self._script_interp.getResponseVals(), time
                )
                self._save_required = True

                # Only re-run the script if there are more images to annotate.
                if self._am.getActiveMediaFile() is not None:
                    self._script_interp.run()
                else:
                    self._accept_annots = False

    @_annotationStateChange
    def undoLastTask(self, time):
        prev_mobj_id = self._am.getActiveMediaFile()

        if len(self._undo_stack) > 0:
            mobj_id, state = self._undo_stack.pop()
            if mobj_id != prev_mobj_id:
                self._am.resetMediaFile(mobj_id, time)
                self._save_required = True

            self._script_interp.restoreStateFromMemento(state)

            self._accept_annots = True

    @_annotationStateChange
    def undoLastMediaObject(self, time):
        # If there is no previous media object on the undo stack, this call
        # will not change the state of the stack.
        img_id, state = self._undo_stack.rewindLastMediaObject(
            self._am.getActiveMediaFile()
        )

        if img_id is not None:
            self._am.resetMediaFile(img_id, time)
            self._save_required = True
            self._script_interp.restoreStateFromMemento(state)

            self._accept_annots = True

    @_annotationStateChange
    def writeAnnotations(self):
        self._am.writeAnnotations()
        self._save_required = False

