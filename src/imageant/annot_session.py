# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import json
from .annot_runner import AnnotationsRunner
from .observable_event import ObservableEvent


# Default file extension filters by media type.
FILE_EXTS = (
    # MEDIA_IMAGES
    # All of these file types are confirmed to work by default on Linux with
    # PyQt5 and Qt 5.9.5. (To get the Qt version, import QT_VERSION_STR from
    # PyQt5.QtCore.)  They are confirmed to work by default on Windows 10 with
    # Pyqt5 and Qt 5.14.1.
    ('.jpg', '.jpeg', '.tif', '.tiff', '.png', '.bmp'),
    # MEDIA_AUDIO
    # All of these file formats are confirmed to work by default on Linux and
    # Windows 10 with SoundFile 0.10.3
    ('.wav', '.wave', '.aiff', '.aif', '.flac', '.ogg')
)


class AnnotationSession:
    """
    Implements session management for media object annotations.  From the
    user's perspective, an annotation session comprises an annotation script, a
    media folder, an output file, and a log file.  To client components, the
    main component of an annotation session is an AnnotationsRunner object.
    """
    # Media type constants.
    MEDIA_IMAGES = 0
    MEDIA_AUDIO = 1

    def __init__(self):
        self.runner = AnnotationsRunner()

        self.sess_file = None
        self.script = None
        self.media_folder = None
        self.media_type = None
        self.csv_out = None
        self.log = None

        # An event that fires whenever the media type changes.  This event is
        # guaranteered to fire before any events caused by changes to the
        # AnnotationsRunner, which ensures that observers have an opportunity
        # to make media type-specific adjustments before handling
        # AnnotationsRunner events.  The observer should be a callable that
        # accepts a single argument, which will be the new media type (i.e.,
        # one of the media type constants or None).
        self.mediaTypeChange = ObservableEvent()

    def getRunner(self):
        return self.runner

    def getSessionFile(self):
        return self.sess_file

    def getScriptFile(self):
        return self.script

    def getMediaFolder(self):
        return self.media_folder

    def getMediaType(self):
        return self.media_type

    def getMediaTypeStr(self):
        if self.media_type == self.MEDIA_IMAGES:
            return 'image'
        elif self.media_type == self.MEDIA_AUDIO:
            return 'audio file'
        else:
            raise Exception('Annotation session media type not set.')

    def getOutputFile(self):
        return self.csv_out

    def getLogFile(self):
        return self.log

    def updateSession(self, sess_file, script_file, media_folder, media_type):
        """
        Updates the session using the provided session file path, script file
        path, and media folder path.
        """
        if media_type not in (self.MEDIA_IMAGES, self.MEDIA_AUDIO):
            raise ValueError('Invalid media type.')

        self.sess_file = os.path.abspath(sess_file)
        self.script = os.path.abspath(script_file)
        self.media_folder = os.path.abspath(media_folder)
        old_media_type = self.media_type
        self.media_type = media_type

        self.saveSession()

        if old_media_type != self.media_type:
            self.mediaTypeChange(self.media_type)

        self._initSession(False)

    def saveSession(self):
        sess_data = {
            'script': self.script,
            'media_folder': self.media_folder,
            'media_type': self.media_type
        }

        with open(self.sess_file, 'w') as fout:
            json.dump(sess_data, fout)

    def restoreSession(self, sess_file):
        """
        Reloads a session from the provided session file.
        """
        with open(sess_file) as fin:
            try:
                sess_data = json.load(fin)
            except json.decoder.JSONDecodeError as err:
                raise Exception(
                    'The file "{0}" does not appear to be a valid annotation '
                    'session file.'.format(sess_file)
                )

        if not(os.path.isfile(sess_data['script'])):
            raise Exception(
                'The annotation script, "{0}", could not be found.'.format(
                    sess_data['script']
                )
            )

        if not(os.path.isdir(sess_data['media_folder'])):
            raise Exception(
                'The media folder, "{0}", could not be found.'.format(
                    sess_data['media_folder']
                )
            )

        self.sess_file = sess_file
        self.script = sess_data['script']
        self.media_folder = sess_data['media_folder']
        old_media_type = self.media_type
        self.media_type = sess_data['media_type']

        if old_media_type != self.media_type:
            self.mediaTypeChange(self.media_type)

        self._initSession(True)

    def _initSession(self, restore):
        """
        Initializes the session by creating paths for the output and log files
        and attempts to initialize the AnnotationsRunner with the provided
        script and media folder.

        restore: If True, attempt to restore annotations from the log file.
        """
        parts = os.path.splitext(self.sess_file)
        self.csv_out = parts[0] + '.csv'
        self.log = parts[0] + '.log'

        self.runner.initAnnotations(
            self.script, self.media_folder, self.csv_out, self.log, restore,
            FILE_EXTS[self.media_type]
        )

