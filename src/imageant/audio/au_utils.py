# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import soundfile as sf
import math


def convert_to_mono(audio_data):
    """
    Converts audio data to mono by averaging corresponding samples of the audio
    channels.  Expects a numpy array of shape (samples x channels) or
    (samples).  If the data are a (samples x 1) array, they are returned as a
    1-d vector.  If the data are already a 1-d vector, they are returned
    unaltered.
    """
    sh_vec = audio_data.shape

    if len(sh_vec) == 1:
        return audio_data
    elif len(sh_vec) == 2:
        if sh_vec[1] == 1:
            return audio_data[:,0]
        else:
            return np.mean(audio_data, axis=1)
    else:
        raise Exception('Invalid audio data array shape: {0}.'.format(sh_vec))


class ChoppedAudio:
    """
    Provides a view of an audio file as a sequence of chunks of constant
    length, except for the last chunk, which may be less than the constant
    length.  Chunks can optionally overlap.  Implements a read-only,
    sequence-like interface, where subscript notation can be used to access
    specific audio chunks and instances can be used as iterables.
    """
    def __init__(
        self, input_file, chunk_length, overlap=0.0, force_mono=True, input_max=None
    ):
        """
        input_file: An input audio file.
        chunk_length: The length of each audio chunk, in seconds.
        overlap: The proportional overlap of successive audio chunks.
        force_mono: Convert multi-channel input audio to mono.
        input_max: The maximum length of input audio to use, in seconds.
        """
        if chunk_length <= 0:
            raise Exception('The chunk length must be greater than 0.')
        if overlap < 0.0 or overlap >= 1.0:
            raise Exception('The overlap must be in the range [0.0,1.0).')

        if input_max is not None and not(input_max > 0.0):
            raise Exception(
                'The maximum input stream length must be greater than 0.'
            )

        self.fin = sf.SoundFile(input_file, mode='r')

        self.chunk_length = chunk_length
        self.overlap = overlap
        self.force_mono = force_mono

        if input_max is not None:
            self.max_frames = input_max * self.fin.samplerate
        else:
            self.max_frames = self.fin.frames

        self.samps_per_chunk = round(self.fin.samplerate * self.chunk_length)

        # Calculate the stride between successive chunks.
        self.stride = int(self.samps_per_chunk * (1 - self.overlap))
        if self.stride == 0:
            self.stride = 1

        self.chunk_cnt = self._calcChunkCnt(self.max_frames)

    def __del__(self):
        try:
            self.fin.close()
        except:
            pass

    def getSampleRate(self):
        return self.fin.samplerate

    def getAudioFileFormat(self):
        return self.fin.format

    def getOutputChannels(self):
        if self.force_mono:
            return 1
        else:
            return self.fin.channels

    def getChunksInFile(self):
        """
        Returns the total number of audio chunks in the full input audio file,
        even if not all of the audio file is being used.
        """
        return self._calcChunkCnt(self.fin.frames)

    def __len__(self):
        """
        Returns the total number of audio chunks in the input data, which might
        not include the entire input file.
        """
        return self.chunk_cnt

    def __getitem__(self, index):
        if not(isinstance(index, int)):
            raise TypeError('Invalid index type.')

        if index < 0 or index >= len(self):
            raise IndexError('Index out of range.')
        
        self.fin.seek(index * self.stride)

        return self._getNextChunk()

    def __iter__(self):
        self.fin.seek(0)

        return self

    def __next__(self):
        return self._getNextChunk()

    def _getNextChunk(self):
        start_pos = self.fin.tell()

        if start_pos < self.max_frames:
            sdata = self.fin.read(frames=self.samps_per_chunk)

            if self.overlap > 0.0 and self.fin.tell() < self.max_frames:
                self.fin.seek(start_pos + self.stride)

            if self.force_mono:
                sdata = convert_to_mono(sdata)

            return sdata
        else:
            raise StopIteration()

    def _calcChunkCnt(self, n_frames):
        """
        Calculates the total number of chunks in the given number of audio
        frames.
        """
        if n_frames == 0:
            return 0
        elif self.samps_per_chunk > n_frames:
            return 1
        else:
            return math.ceil(
                (n_frames - self.samps_per_chunk) / self.stride
            ) + 1

