# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtCore import QBuffer, QIODevice, QByteArray
from PyQt5.QtMultimedia import QAudio, QAudioOutput, QAudioFormat
from ..observable_event import ObservableEvent


class AudioPlayer:
    """
    Implements a player for in-memory audio data with a high-level API.  All
    input audio is expected to have a single channel (i.e., monophonic)
    represented as a 1-d numpy array of 32-bit floating point values.
    """
    # State constants.
    STOPPED = 0
    PLAYING = 1
    PAUSED = 2
    NO_AUDIO = 3

    def __init__(self, parent=None):
        """
        parent: The parent QObject, if any.
        """
        # The raw audio data, sample rate, and audio stream duration (in ms).
        self.au_data = None
        self.samp_rate = None
        self.samps_per_ms = None
        self.duration = 0

        self.parent = parent

        self.state = self.NO_AUDIO

        # A flag to indicate if we're in the middle of a seek operation on a
        # stopped audio stream.
        self.seeking = False

        self.audio_format = QAudioFormat()
        self.audio_format.setChannelCount(1)
        self.audio_format.setCodec('audio/pcm')
        self.audio_format.setSampleSize(32)
        self.audio_format.setSampleType(QAudioFormat.Float)
        self.audio_format.setSampleRate(44100)

        self.au_buffer = QBuffer()
        self.au_output = None

        # An event that fires when the contents of the loaded audio stream
        # changes.  The observer should be a callable that does not accept any
        # arguments.
        self.audioChange = ObservableEvent()
            
        # An event that fires when the audio stream starts playing or resumes
        # playback.  The observer should be a callable that does not take any
        # arguments.
        self.playEvent = ObservableEvent()
            
        # An event that fires when the audio stream pauses playback.  The
        # observer should be a callable that does not take any arguments.
        self.pauseEvent = ObservableEvent()
            
        # An event that fires when the audio stream stops playback.  The
        # observer should be a callable that does not take any arguments.
        self.stopEvent = ObservableEvent()
            
        # An event that fires whenever the playback position changes.  The
        # observer should be a callable that accepts a single argument, which
        # will be the new playback position in milliseconds.
        self.positionChange = ObservableEvent()

        # An event that fires when an audio system error is encountered.  The
        # observer should be a callable that accepts a single argument, which
        # will be the error message.
        self.audioError = ObservableEvent()

    def getState(self):
        return self.state

    def setAudio(self, au_data, samp_rate):
        """
        Sets the input audio, which must either be a 1-d numpy array of 32-bit
        floating point values or None.

        au_data: The in-memory audio samples or None.
        samprate (int): The sample rate in samples per second.
        """
        old_pos_ms = self.getPosition()

        self.au_data = au_data
        self.samp_rate = samp_rate

        if self.state not in (self.STOPPED, self.NO_AUDIO):
            self.stop()

        if self.au_buffer.isOpen():
            self.au_buffer.close()

        if au_data is None:
            self.au_data = None
            self.samp_rate = None
            self.samps_per_ms = None
            self.duration = 0
            self.au_buffer.setData(QByteArray())
            self.state = self.NO_AUDIO
        else:
            self.samps_per_ms = samp_rate / 1000
            self.duration = len(self.au_data) / self.samps_per_ms

            if (
                self.samp_rate != self.audio_format.sampleRate() or
                self.au_output is None
            ):
                self.audio_format.setSampleRate(self.samp_rate)
                self._createAudioOutput()

            self.au_buffer.setData(QByteArray(self.au_data.tobytes()))
            self.au_buffer.open(QIODevice.ReadOnly)
            self.au_buffer.seek(0)
            self.state = self.STOPPED

        self.audioChange()

        if old_pos_ms != 0:
            self.positionChange(0)

    def getAudioData(self):
        """
        Returns the array of raw audio data.
        """
        return self.au_data

    def getDuration(self):
        """
        Returns the duration of the current audio stream, in milliseconds.
        """
        return self.duration

    def play(self):
        """
        Begin or resume playback of the audio data.
        """
        # Because QAudioOutput.reset() does not trigger a stateChanged signal
        # on Windows 10 (which seems to be a bug), we have to set the
        # self.seeking flag here to ensure that subsequent attempts to stop
        # playback are correctly processed.
        self.seeking = False

        if self.au_output.state() == QAudio.SuspendedState:
            self.au_output.resume()
        else:
            self.au_output.start(self.au_buffer)

    def pause(self):
        """
        Pause playback of the audio data.
        """
        self.au_output.suspend()

    def stop(self):
        """
        Stop playback of the audio data.  The audio stream position is reset to
        the beginning of the stream.
        """
        if self.au_output.state() != QAudio.StoppedState:
            self.au_output.stop()
        elif self.state != self.STOPPED:
            # This is required if the QAudioOutput is already in the stopped
            # state due to a call to reset() as part of a seek operation.
            self._stopStream()

    def getPosition(self):
        """
        Returns the current playback position, in milliseconds.
        """
        if self.au_data is None:
            return 0

        # Calculate the current playback position in bytes.
        # This is calculated as the current position in the read buffer
        # (representing all bytes that have been delivered to the audio buffer)
        # minus the number of bytes currently sitting in the audio buffer
        # (i.e., the bytes that have not yet been played).
        pos_bytes = (
            self.au_buffer.pos() -
            (self.au_output.bufferSize() - self.au_output.bytesFree())
        )

        return (pos_bytes / 4) / self.samps_per_ms

    def setPosition(self, new_pos):
        """
        Sets the current playback position.

        new_pos: The new playback position, in milliseconds.
        """
        new_pos_bytes = int(new_pos * self.samps_per_ms) * 4

        if self.au_output.state() == QAudio.SuspendedState:
            # Clear the audio buffer if the stream is paused.  This prevents
            # any leftover audio samples in the buffer from playing when
            # playback resumes.  On Windows and Linux (at least), a reset()
            # causes a QAudioOutput state change to stopped, so we use the
            # self.seeking flag to indicate that the stream should not actually
            # be considered stopped.  Oddly, on Windows 10, this change to the
            # stopped state does not trigger the QAudioOutput's stateChanged
            # signal, which appears to be a bug.
            self.seeking = True
            self.au_output.reset()

        self.au_buffer.seek(new_pos_bytes)

        self.positionChange(new_pos)

    def _createAudioOutput(self):
        """
        Creates an audio output that accepts the current sample rate.
        """
        if self.au_output is not None:
            del self.au_output

        self.au_output = QAudioOutput(self.audio_format, self.parent)
        self.au_output.setNotifyInterval(100)
        self.au_output.notify.connect(self._audioOutputNotify)
        self.au_output.stateChanged.connect(self._stateChanged)

    def _stopStream(self):
        self.state = self.STOPPED
        self.au_buffer.seek(0)
        self.stopEvent()
        self.positionChange(0)

    def _audioOutputNotify(self):
        """
        Responds to notify signals from a QAudioOutput.
        """
        self.positionChange(self.getPosition())

    def _stateChanged(self, state):
        """
        Responds to stateChanged signals from a QAudioOutput.
        """
        #print(state)
        if state == QAudio.ActiveState:
            # Triggered when the player begins reading data from the buffer.
            self.state = self.PLAYING
            self.playEvent()
        elif state == QAudio.SuspendedState:
            # Triggered when playback is paused.
            self.state = self.PAUSED
            self.pauseEvent()
        elif state == QAudio.IdleState:
            # Triggered when the end of the stream is reached.
            self.positionChange(self.getPosition())
            self.au_output.stop()
        elif state == QAudio.StoppedState:
            if self.au_output.error() == QAudio.NoError:
                if not(self.seeking):
                    self._stopStream()
                else:
                    self.seeking = False
            # All errors also set StoppedState.
            elif self.au_output.error() == QAudio.OpenError:
                self.audioError(
                    'Audio device error: could not play the audio stream.'
                )
            elif self.au_output.error() == QAudio.IOError:
                self.audioError(
                    'I/O error encountered while playing the audio stream.'
                )
            elif self.au_output.error() == QAudio.FatalError:
                self.audioError(
                    'Unrecoverable error encountered while playing the audio '
                    'stream.'
                )
            else:
                self.audioError(
                    'Error encountered while playing the audio stream.'
                )

