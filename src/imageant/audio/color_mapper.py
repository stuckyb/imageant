# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtGui import QColor, QPixmap, QImage, QPainter
import math
import numpy as np


class ColorMapper:
    """
    Implements color mappings for spectrograms.
    """
    def __init__(self):
        # Color gradient definitions for gradient-based mappings.  Gradients
        # can have an arbitrary number of color points.
        self.gradients = {
            'audacity': [
                [0.75, 0.75, 0.75],
                [0.3, 0.6, 1.0],
                [0.9, 0.1, 0.9],
                [1.0, 0.0, 0.0],
                [1.0, 1.0, 1.0]
            ],
            'default': [
                [1.0, 1.0, 1.0],    # white
                [0.0, 0.06, 0.94],  # blue
                [0.0, 0.64, 0.2],   # green
                [0.92, 0.9, 0],     # yellow
                [1.0, 0.06, 0]      # red
            ],
            # This is an experimental work in progress.
            'raven': [
                [0.0, 0.0, 0.56],
                [0.0, 1.0, 1.0],
                #[0.55, 1.0, 0.45],
                [0.81, 1.0, 0.19],
                [1.0, 0.88, 0.0],
                [1.0, 0.35, 0.0],
                [0.51, 0.0, 0.0],
            ],
        }

        # A dictionary of single-value color mapping functions (i.e., for
        # mapping one spectrogram intensity value with each call).
        self.singlepoint_mappings = {
            'grayscale': self.mapToGrayscale,
            'rainbow': self.mapToRainbow,
        }

        # A dictionary of multi-value color mapping functions (i.e., for
        # mapping multiple spectrogram intensity value with a single call).
        self.multipoint_mappings = {
            'grayscale': self.mapToColorsMulti
        }

        # Add the gradient mappings to the function dictionaries.
        for gradient in self.gradients:
            self.singlepoint_mappings[gradient] = self.mapToGradient
            self.multipoint_mappings[gradient] = self.mapToColorsMulti

        # Holds the current color mapping and gradient.
        self.mapping = None
        self.gradient = None

        # Specifies whether the color map should be inverted.
        self.inverted = False

        # A reference to the single-value mapping function for the current
        # color mapping.
        self._getColor = None

        # A reference to the multi-value mapping function for the current color
        # mapping.
        self._getColors = None

        # Cached lookup table of color mapping values for multi-value
        # spectrogram mapping.
        self._colormap_vals = None

        # Set the default mapping function.
        self.setMapping('audacity')

    def getAvailableMappings(self):
        return self.singlepoint_mappings.keys()

    def getMapping(self):
        return self.mapping

    def setMapping(self, mappingname):
        if self.mapping == mappingname:
            return

        if mappingname not in self.singlepoint_mappings:
            raise(NameError('Invalid color mapping name.'))

        # If this is a gradient-based mapping, set the appropriate gradient.
        if mappingname in self.gradients:
            self.gradient = self.gradients[mappingname]
        else:
            self.gradient = None

        self.mapping = mappingname
        self._colormap_vals = None

        self._setMappingFuncs()

    def getInverted(self):
        return self.inverted

    def setInverted(self, inverted):
        self.inverted = inverted
        self._setMappingFuncs()

    def _setMappingFuncs(self):
        """
        Sets references to the sigle-point and multi-point color mapping
        functions for the current color mapping.
        """
        # Set the single-value mapping function.
        if not(self.inverted):
            self._getColor = self.singlepoint_mappings[self.mapping]
        else:
            self._getColor = self.invertMappingSingle
            self.__getColor = self.singlepoint_mappings[self.mapping]

        # Set the multi-value mapping function.
        if self.mapping in self.multipoint_mappings:
            self._getColors = self.multipoint_mappings[self.mapping]
        else:
            self._getColors = None

    def getMappingThumbnail(self, mappingname, width, height):
        """
        Get a thumbnail image of the specified color mapping.
        """
        if mappingname not in self.mappings:
            raise(NameError('Invalid color mapping name.'))

        # Save the old mapping before setting the new one.
        oldmname = self.getMapping()
        self.setMapping(mappingname)

        thumbvals = []
        for y in range(0, height):
            thumbvals.append([])
            for x in range(0, width):
                thumbvals[y].append(float(x) / (width-1))

        # Get the raw thumbnail image data.
        img = self.getSpectrogramImage(thumbvals, 0, 0)

        # Restore the old mapping.
        self.setMapping(oldmname)

        return img

    def getSpectrogramImage(self, specgram, startspacepx, endspacepx):
        """
        Returns a QImage containing the color-mapped spectrogram data.

        specgram (float, [0.0-1.0]): A 2-d numpy array of spectrogram intensity
            values.
        startspacepx (int): Padding to add on the left side of the image (for
            precise time synchronization, e.g.).
        endspacepx (int): Padding to add on the right side of the image.
        """
        # The multi-value mapping implementation is by far the fastest.  See
        # color_map_test.py.
        return self.getMappedImgData_Multi(specgram, startspacepx, endspacepx)

    def getMappedImgData_QPixmap(self, specgram, startspacepx, endspacepx):
        """
        Generates the spectrogram image by painting directly on a QPixmap.

        specgram (float, [0.0-1.0]): A 2-d numpy array of spectrogram intensity
            values.
        startspacepx (int): Padding to add on the left side of the image (for
            precise time synchronization, e.g.).
        endspacepx (int): Padding to add on the right side of the image.
        """
        ylen = len(specgram)
        xlen = len(specgram[0])

        # Repeatedly use a single QColor object instead of allocating thousands
        # of QColor objects.
        color = QColor()

        pm = QPixmap(xlen + startspacepx + endspacepx, ylen)
        pm.fill()
        painter = QPainter(pm)

        #for y in range(ylen - 1, -1, -1):
        for y in range(0, ylen, 1):
            # Add the row image data.
            for x in range(xlen):
                cvals = self._getColor(specgram[y][x])
                color.setRgb(*cvals)
                painter.setPen(color)
                painter.drawPoint(x + startspacepx, (ylen - 1) - y)

        return pm

    def getMappedImgData_QImage(self, specgram, startspacepx, endspacepx):
        """
        Generates the spectrogram image by painting directly on a QImage.

        specgram (float, [0.0-1.0]): A 2-d numpy array of spectrogram intensity
            values.
        startspacepx (int): Padding to add on the left side of the image (for
            precise time synchronization, e.g.).
        endspacepx (int): Padding to add on the right side of the image.
        """
        ylen = len(specgram)
        xlen = len(specgram[0])

        # Repeatedly use a single QColor object instead of allocating thousands
        # of QColor objects.
        color = QColor()

        img = QImage(
            xlen + startspacepx + endspacepx, ylen, QImage.Format_RGB32
        )
        img.fill(QColor(255, 255, 255))
        painter = QPainter(img)

        #for y in range(ylen - 1, -1, -1):
        for y in range(0, ylen, 1):
            # Add the row image data.
            for x in range(xlen):
                cvals = self._getColor(specgram[y][x])
                color.setRgb(*cvals)
                painter.setPen(color)
                painter.drawPoint(x + startspacepx, (ylen - 1) - y)

        return img

    def getMappedImgData_Bytearray(self, specgram, startspacepx, endspacepx):
        """
        Generates the spectrogram image by using bytearray to build an unsigned
        char array of RGB values, then converting to a QImage.

        specgram (float, [0.0-1.0]): A 2-d numpy array of spectrogram intensity
            values.
        startspacepx (int): Padding to add on the left side of the image (for
            precise time synchronization, e.g.).
        endspacepx (int): Padding to add on the right side of the image.
        """
        ylen = len(specgram)
        xlen = len(specgram[0])

        img_width = xlen + startspacepx + endspacepx

        # Among various methods of generating a low-level array of unsigned
        # chars, using extend() on a bytearray seems to be the fastest.  See
        # char_array_test.py.
        imgvals = bytearray()
        for y in range(ylen - 1, -1, -1):
            # Add the spacing at the beginning of the row.
            for i in range(startspacepx):
                imgvals.extend((255, 255, 255))
            # Add the row image data.
            for x in range(xlen):
                
                imgvals.extend(self._getColor(specgram[y][x]))
            # Add the spacing at the end of the row.
            for i in range(endspacepx):
                imgvals.extend((255, 255, 255))

        img = QImage(
            imgvals, img_width, ylen, img_width * 3,
            QImage.Format_RGB888
        )

        return img

    def getMappedImgData_Multi(self, specgram, startspacepx, endspacepx):
        """
        Generates the spectrogram image using the multi-value spectrogram
        mapping functions.  Returns a QImage containing the mapped image.

        specgram (float, [0.0-1.0]): A 2-d numpy array of spectrogram intensity
            values.
        startspacepx (int): Padding to add on the left side of the image (for
            precise time synchronization, e.g.).
        endspacepx (int): Padding to add on the right side of the image.
        """
        imgvals = self._getColors(specgram, startspacepx, endspacepx)

        img_width = specgram.shape[1] + startspacepx + endspacepx
        img = QImage(
            imgvals, img_width, specgram.shape[0], img_width * 3,
            QImage.Format_RGB888
        )

        return img

    def invertMappingSingle(self, intensity):
        return self.__getColor(1.0 - intensity)

    def mapToGrayscale(self, intensity):
        # Map the intensity to a corresponding grayscale value.
        cval = int((1 - intensity) * 255)
        #print cval

        return (cval, cval, cval)

    def mapToRainbow(self, intensity, color):
        # Calculate the hue so that an intensity of 0 maps to primary blue and
        # an intensity of 1 maps to purple.
        if intensity < 0.67:
            hue = -1 * intensity + 0.67
        else:
            hue = -0.531 * intensity + 1.361

        color = QColor.fromHsv((hue * 255), 255, 255)

        return color.getRgb()[0:3]

    def mapToRaven(self, intensity, color):
        # Experimental.  Calculate the hue so that an intensity of 0 maps to
        # hue 240, value 142, and an intensity of 1.0 maps to hue 0, value 142,
        # with intermediate values mapping to higher intensities.  Use a
        # sigmoid curve for the mapping.
        hue = int(
            (1 / (1 + math.exp(-8 * ((1.0 - intensity) - 0.5)))) * 240
        )
        value = int(113 * math.sin(intensity * math.pi) + 142)

        color = QColor.fromHsv(hue, 255, value)

        return color.getRgb()[0:3]

    def mapToGradient(self, intensity):
        """
        A pure Python implementation of color gradient mapping.

        intensity (float, [0.0-1.0]): A single spectrogram intensity value.
        """
        gradient = self.gradient

        endindex = len(gradient) - 1

        start = int(intensity * endindex)
        if start < endindex:
            end = start + 1
        else:
            end = start

        midpnt = (intensity * endindex) - start
        return (
            int(
                ((gradient[end][0] - gradient[start][0]) * midpnt +
                gradient[start][0]) * 255
            ),
            int(
                ((gradient[end][1] - gradient[start][1]) * midpnt +
                gradient[start][1]) * 255
            ),
            int(
                ((gradient[end][2] - gradient[start][2]) * midpnt +
                gradient[start][2]) * 255
            )
        )

    def _buildGrayscaleArray(self, length):
        """
        Returns a numpy array of evenly spaced color values along a simple
        grayscale color mapping gradient.  The array will contain exactly
        length color values.
        """
        cmvals = np.empty((length, 3), np.uint8)

        channel_vals = np.around(
            np.linspace(1.0, 0.0, num=length, endpoint=True) * 255
        )
        for channel in range(3):
            cmvals[:,channel] = channel_vals

        return cmvals

    def _buildGradientArray(self, length):
        """
        Returns a numpy array of evenly spaced color values along the current
        color mapping gradient.  The array will contain approximately length
        color values.
        """
        if self.gradient is None:
            raise Exception('No color gradient specified.')

        gradient = self.gradient

        # The number of color intervals in the gradient.
        gradsteps = len(gradient) - 1

        # Construct an array of ~length evenly-spaced RGB color values along the
        # continuous color gradient.  We will map all spectrogram intensity
        # values to color values in this array.
        seg_len = round(length / gradsteps)
        gradvals = np.empty((seg_len * gradsteps + 1, 3), np.uint8)
        for channel in range(3):
            for step in range(gradsteps):
                start_i = step * seg_len

                gradvals[start_i:start_i+seg_len+1,channel] = np.around(
                    np.linspace(
                        gradient[step][channel], gradient[step+1][channel],
                        num=seg_len+1, endpoint=True
                    ) * 255
                )

        #print(gradvals)
        #print(gradvals.shape)
        #print(gradvals.size)

        return gradvals

    def _buildColorMappingArray(self, length):
        """
        Generates a new color values array for the current color mapping.
        """
        if self.gradient is not None:
            return self._buildGradientArray(length)
        elif self.mapping == 'grayscale':
            return self._buildGrayscaleArray(length)
        else:
            raise NotImplementedError()

    def mapToColorsMulti(
        self, spec, startspacepx, endspacepx, lookup_table_size=400
    ):
        """
        Maps spectrogram intensity values to an array of colors that represents
        the current color mapping.
        """
        if self._colormap_vals is None:
            self._colormap_vals = self._buildColorMappingArray(
                lookup_table_size
            )

        cmvals = self._colormap_vals
        if self.inverted:
            cmvals = np.flipud(cmvals)

        # Add the end spaces, if needed.
        if startspacepx > 0 or endspacepx > 0:
            spec = np.pad(
                spec, ((0,0), (startspacepx, endspacepx)), mode='constant',
                constant_values=0.0
            )

        # Calculate the index into the color map array for the color that maps
        # to each intensity value in the spectrogram.
        cmvals_indices = np.around(
            np.ravel(np.flipud(spec), order='C') * (cmvals.shape[0] - 1)
        ).astype(np.int32)

        # Build the array of color values for each spectrogram intensity value.
        imgvals = np.empty((spec.size, 3), np.uint8, order='C')
        for channel in range(3):
            imgvals[:,channel] = cmvals[cmvals_indices,channel]

        return imgvals

