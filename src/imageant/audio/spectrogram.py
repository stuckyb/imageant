# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import math
from .color_mapper import ColorMapper
# These only need to be imported for performance benchmarking.
#from matplotlib import mlab
#import librosa


class Spectrogram:
    def __init__(self):
        self.sdata = None
        self.timeinfo = None

        # The starting and ending audio samples to use.
        self.start_samp = 0     # The first audio sample to use.
        self.end_samp = 0       # One past the last audio sample to use.

        # STFT settings.
        self.winsize = 1024
        self.overlap = 0.5

        self.winfunc = FFTWindowFunction()
        self.winfunc.setFunction(FFTWindowFunction.HAMMING)

        # Spectrogram settings.
        self.usedBthreshold = True
        self.dBthreshold = -44
        self.gain = 4

        self.mapper = ColorMapper()

    def setInputData(self, sound_data):
        """
        sound_data: A 1-D numpy array of floating-point audio samples with
        values in the range [-1.0, 1.0].  No DC offset should be present (i.e.,
        the mean of the samples should be 0).
        """
        self.sdata = sound_data

        self.start_samp = 0
        self.end_samp = len(self.sdata)

        self._calcSpec()

    def getSound(self):
        return self.sdata

    def getSpectrogramData(self):
        """
        Returns a tuple with two components: the actual spectrogram data,
        with frequency as the 1st index and time as the 2nd; and a list of time
        information containing three values: 1) the number of samples covered
        by each FFT slice, 2) the sample at the middle of the first slice, and
        3) the sample at the middle of the last slice.
        """
        return (self.sdata, self.timeinfo)

    def getWindowSize(self):
        return self.winsize

    def getWindowFunction(self):
        return self.winfunc.getFunction()

    def getOverlap(self):
        return self.overlap

    def getUseDBThreshold(self):
        return self.usedBthreshold

    def getDBThreshold(self):
        return self.dBthreshold

    def getGain(self):
        return self.gain

    def setFFTParameters(
        self, winsize=0, winfunc=1, overlap=1, usedBthreshold=None,
        dBthreshold=0, gain=-1
    ):
        if winsize > 0:
            self.winsize = winsize
        if winfunc >= 0 and winfunc < len(FFTWindowFunction.function_names):
            self.winfunc.setFunction(winfunc)
        if overlap >= 0 and overlap < 1:
            self.overlap = overlap
        if usedBthreshold != None:
            self.usedBthreshold = usedBthreshold
        if dBthreshold < 0:
            self.dBthreshold = dBthreshold
        if gain >= 0:
            self.gain = gain

        # Recalculate the spectrogram.
        if self.sdata is not None:
            self.calcSpec()

    def setTimeRange(self, startsamp, endsamp):
        if (
            startsamp < endsamp and
            startsamp >= 0 and
            endsamp <= len(self.sdata)
        ):
            self.start_samp = startsamp
            self.end_samp = endsamp
        else:
            raise Exception(
                'Invalid time range: {0}, {1}.'.format(startsamp, endsamp)
            )

    def getColorMapping(self):
        return self.mapper.getMapping()

    def setColorMapping(self, mappingname):
        self.mapper.setMapping(mappingname)
        self.createBaseImage()

        self.notifyParentChildChanged()

    def getColorMapper(self):
        return self.mapper

    def getColorMapInverted(self):
        return self.mapper.getInverted()

    def setColorMapInverted(self, inverted):
        self.mapper.setInverted(inverted)

    def printSpec(self, specgram):
        print(specgram[:,0])
        print(len(specgram[:,0]))
        print(specgram.min())
        print(specgram.max())

    def _calcSpec(self):
        """
        Calculates the short-time FFT for the spectrogram of the audio data.
        """
        if len(self.sdata) < self.winsize:
            # We don't have enough data to calculate a spectrogram.
            self.sdata = None
            return

        overlapsamps = int(self.overlap * self.winsize)
        stride = self.winsize - overlapsamps

        # See if we can take extra data at the beginning of the sample range to
        # compensate for coverage lost due to overlapping the FFT windows.  For
        # plotting purposes, we consider each FFT "slice" to cover a range of
        # audio samples equal to stride, which means that if
        # stride < winsize, some samples will be "lost" from the first
        # FFT slice.
        startadj = self.winsize // 2 - (stride // 2)
        #print startadj
        if (self.start_samp - startadj) < 0:
            startadj = 0
        #startadj = 0

        # Also see if we can take extra data at the end of the sample range to
        # increase coverage. This calculation is considerably more complicated.
        # The algorithm is as follows:  First, determine the last sample
        # covered by the spectrogram, accounting for window overlaps.  Then,
        # figure out how many more stride lengths could fit in the
        # uncovered audio at the end of the range, and try to add this many
        # samples to the end of the range.
        numwins = (
            ((self.end_samp - self.start_samp + startadj) -
            self.winsize) // stride + 1
        )
        coverend = (
            self.start_samp - startadj + self.winsize // 2 +
            stride * (numwins - 1) +
            (stride // 2)
        )
        endadj = ((self.end_samp - coverend) // stride) * stride
        #print endadj
        if (self.end_samp + endadj) > len(self.sdata):
            endadj = 0
        #endadj = 0

        # Get the audio data and calculate the spectrogram.
        data = self.sdata[self.start_samp-startadj:self.end_samp+endadj]
        specgram = self._getSpecData_NumpyStrided(data, stride)

        # Convert the power values to dB, setting the maximum power at 0 dB.
        # If a power value is actually 0.0, log10 is -infinity and the call to
        # log10() will trigger a divide by 0 warning.  We ignore the warning
        # and then deal with any -infinity values afterwards.
        with np.errstate(divide='ignore'):
            specgram = 10 * np.log10(specgram / specgram.max())
        np.nan_to_num(specgram, copy=False)
        #self.printSpec(specgram)

        # Apply the gain.
        specgram = specgram + self.gain

        # Set a minimum dB depending on whether a dB threshold is set.
        mindB = specgram.min()
        if self.usedBthreshold and mindB < self.dBthreshold:
            mindB = self.dBthreshold
        
        # Apply the minimum dB filtering and clip the upper range to compensate
        # for the gain.
        specgram = np.clip(specgram, mindB, 0)

        # Normalize the values to the range 0 to 1.0.
        specgram = 1 - specgram / mindB
        #self.printSpec(specgram)

        # Build the list of time values (in samples).
        #print time
        #print ((time[1] - time[0]), time[0], time[numwins-1])
        time = [
            stride, 
            self.start_samp - startadj + self.winsize // 2,
            self.start_samp - startadj + self.winsize // 2 + stride * (numwins - 1)
        ]
        #time = [i * (1.0 / self.sdata.getSampleRate()) for i in time]
        #print time

        self.sdata = specgram
        self.timeinfo = time

    def _getSpecData_Numpy(self, data, stride):
        """
        This method implements a straightforward algorithm for the STFT using
        numpy operations and a loop over each FFT window.  Although the code is
        relatively easy to understand, it is not as efficient as the numpy
        implementation using array views.
        """
        numfreqs = self.winsize // 2 + 1

        # Get the scale values for the selected window function.
        wf_vals = self.winfunc.getFunctionValues(self.winsize)

        numwins = (len(data) - self.winsize) // stride + 1
        specgram = np.zeros((numfreqs, numwins))

        winnum = 0
        for i in range(0, len(data) - self.winsize + 1, stride):
            # Get the next segment of audio samples and multiply it by the
            # window function.
            window = data[i:i+self.winsize]
            window = window * wf_vals

            # Calculate the FFT.
            ft = np.fft.rfft(window, n=self.winsize)

            # Calculate the square of the frequency magnitudes (power).  This
            # can also be calculated (perhaps more transparently) as:
            # pt = ft.real**2 + ft.imag**2.  However, this method takes about
            # 1.4 times longer than using the complex conjugate.
            specgram[:,winnum] = (np.conj(ft) * ft).real
            winnum += 1

        # Note that matplotlib scales the power data by the norm (length) of
        # the window function values to compensate for windowing loss:
        # specgram /= (wf_vals**2).sum().  However, since we're converting
        # everything to dB anyway, we can skip this step.

        # Matplotlib also scales everything but the DC and winsize/2 components
        # by 2 to compensate for calculating a 1-sided density, so do that.
        specgram[1:-1] *= 2

        return specgram

    def _getSpecData_NumpyStrided(self, data, stride):
        """
        This method implements the STFT using numpy array views.  It is more
        time-efficient than the straightforward implementation, but also
        significantly more difficult to understand because it depends on numpy
        array internals.  I have tried to add documentation that thoroughly
        explains each step of the algorithm.  This implementation is similar to
        the approach used internally by matplotlib, but it is substantially
        faster because it uses a real-valued FFT and avoids some
        computations/manipulations not required for building a dB spectrogram.
        """
        # Get the scale values for the selected window function.
        wf_vals = self.winfunc.getFunctionValues(self.winsize)

        numwins = (len(data) - self.winsize) // stride + 1

        # We want to create a view on the array that has the window size on
        # axis 0 and the total number of windows on axis 1.  In other words, a
        # matrix in which each column is a window on which to run the FFT.
        v_shape = (self.winsize, numwins)

        # Next we define how the array data should be mapped to the new shape.
        # For the first dimension (axis), we use the length, in bytes, of a
        # single array element (data.strides[0]).  For the second dimension
        # (axis), we skip by the number of samples to jump in each movement of
        # the sliding FFT window.  In other words, as we move down each column
        # of the array (i.e., a single FFT window), we advance the array
        # pointer by a single element.  As we move across a row (i.e., from one
        # window to the next), we advance the array pointer by the number of
        # samples in each jump of the sliding window.
        v_strides = (data.strides[0], stride * data.strides[0])

        # Create the array view with one FFT window per column.
        win_view = np.lib.stride_tricks.as_strided(
            data, shape=v_shape, strides=v_strides, writeable=False
        )

        # Reshape the windowing values into a column vector and apply it to
        # each FFT window.  In numpy jargon, we "broadcast" the column vector
        # across the columns (i.e., each window) of the array view.
        windowed = win_view * wf_vals.reshape((-1, 1))

        # Calculate the FFT.
        ft = np.fft.rfft(windowed, n=self.winsize, axis=0)

        # Calculate the square of the frequency magnitudes (power).  This
        # can also be calculated (perhaps more transparently) as:
        # pt = ft.real**2 + ft.imag**2.  However, this method takes about
        # 1.4 times longer than using the complex conjugate.
        specgram = (np.conj(ft) * ft).real

        # Note that matplotlib scales the power data by the norm (length) of
        # the window function values to compensate for windowing loss:
        # specgram /= (wf_vals**2).sum().  However, since we're converting
        # everything to dB anyway, we can skip this step.

        # Matplotlib also scales everything but the DC and winsize/2 components
        # by 2 to compensate for calculating a 1-sided density, so do that.
        specgram[1:-1] *= 2

        return specgram

    def _getSpecData_Matplotlib(self, data, stride):
        # Get the scale values for the selected window function.
        wf_vals = self.winfunc.getFunctionValues(self.winsize)

        specgram, freq, time = mlab.specgram(data,
                NFFT=self.winsize, Fs=44100, window=wf_vals,
                noverlap=(self.winsize - stride), scale_by_freq=False)

        return specgram

    def _getSpecData_Librosa(self, data, stride):
        # Get the scale values for the selected window function.
        wf_vals = self.winfunc.getFunctionValues(self.winsize)

        fft_data = librosa.core.stft(
            data, n_fft=self.winsize, hop_length=stride, window=wf_vals,
            center=False
        )
        specgram = (np.conj(fft_data) * fft_data).real

        return specgram

    def getImage(self):
        """
        Returns an image of the spectrogram drawn onto an off-screen buffer.
        """
        if self.sdata is None:
            return None

        # If the FFT windows overlap and there was no data available to pad the
        # calculations on either end, then there will be a "buffer" of time
        # with no data on each side of the spectrogram.  We need to calculate
        # how many timesteps to leave blank in order to render the spectrogram
        # so that the image is properly in sync with the time axis.  The
        # "timestep" is the amount of time covered by one spectrogram slice (in
        # samples).
        timestep = self.timeinfo[0]
        # Calculate how many timesteps fit into the initial time not covered by
        # the first spectrogram slice
        # (this time is self.times[1] - self.start_samp - timestep / 2).
        startbuffersteps = (self.timeinfo[1] - self.start_samp - timestep // 2) // timestep
        #print(startbuffersteps)
        # Do the same calculation for the time not covered at the end of the
        # spectrogram.
        endbuffersteps = (self.end_samp - self.timeinfo[2] - timestep // 2) // timestep
        #print (self.end_samp - self.times[2] - timestep // 2)
        #print (timestep // 2)
        #print(endbuffersteps)

        # Draw the spectrogram onto an offscreen buffer.  Each pixel represents
        # one time/frequency power value.
        pm = self.mapper.getSpectrogramImage(
            self.sdata, startbuffersteps, endbuffersteps
        )

        return pm


class FFTWindowFunction:
    """
    Implements the window functions used for FFT analysis.
    """
    # Window function constants.
    RECTANGULAR = 0
    HAMMING = 1
    HANN = 2
    BARTLETT = 3
    BLACKMAN = 4
    BLACKMAN_HARRIS = 5
    WELCH = 6

    # A list mapping window function constants to strings
    # of the function names.
    function_names = [
            "rectangular", "Hamming", "Hann", "Bartlett", "Blackman",
            "Blackman-Harris", "Welch"
            ]

    def __init__(self):
        self.func = self.HAMMING

    def getFunction(self):
        return self.func

    def setFunction(self, function_id):
        self.func = function_id

    def getFunctionValues(self, winsize):
        """
        Returns the values of the current window function for the given window
        size as a np array.
        """
        # Get the scale values for the selected window function.
        if self.func == self.RECTANGULAR:
            wf_vals = np.ones(winsize)
        elif self.func == self.HAMMING:
            wf_vals = np.hamming(winsize)
        elif self.func == self.HANN:
            wf_vals = np.hanning(winsize)
        elif self.func == self.BARTLETT:
            wf_vals = np.bartlett(winsize)
        elif self.func == self.BLACKMAN:
            wf_vals = np.blackman(winsize)
        elif self.func == self.BLACKMAN_HARRIS:
            a0 = 0.35875
            a1 = 0.48829
            a2 = 0.14128
            a3 = 0.01168
            wf_vals = np.arange(0, winsize, dtype=np.float64)
            term1 = a1 * np.cos((2 * math.pi * wf_vals) / (winsize - 1))
            term2 = a2 * np.cos((4 * math.pi * wf_vals) / (winsize - 1))
            term3 = a3 * np.cos((6 * math.pi * wf_vals) / (winsize - 1))
            wf_vals = a0 - term1 + term2 - term3
        elif self.func == self.WELCH:
            wf_vals = np.arange(0, winsize, dtype=np.float64)
            num = (winsize - 1) / 2.0
            denom = (winsize + 1) / 2.0
            wf_vals = 1.0 - ((wf_vals - num) / denom)**2

        return wf_vals

