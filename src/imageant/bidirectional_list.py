# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class BidirectionalList:
    """
    Implements a list-like data structure that allows reverse lookups of index
    numbers from list items.  Given the very specific use case for ImageAnt, it
    supports only a subset of the typical list operations: append(), len(),
    str(), sort(), iteration, and item lookup in both directions.
    """
    def __init__(self, iterable=None):
        self.ldata = []
        self.reverse = {}

        if iterable is not None:
            for item in iterable:
                self.append(item)

    def append(self, new_item):
        self.ldata.append(new_item)
        if new_item in self.reverse:
            raise ValueError(
                'All items in a BidirectionalList must be unique (or have a '
                'unique hash value).'
        )
        else:
            self.reverse[new_item] = len(self.ldata) - 1

    def __str__(self):
        return str(self.ldata)

    def __len__(self):
        return len(self.ldata)

    def __getitem__(self, index):
        """
        Implements list item lookup by index.
        """
        return self.ldata[index]

    def r(self, value):
        """
        Implements index lookup by list item.
        """
        return self.reverse[value]

    def __iter__(self):
        return iter(self.ldata)

    def sort(self, *args, **kwargs):
        self.ldata.sort(*args, **kwargs)

        # Update the reverse lookup map.
        for index, value in enumerate(self.ldata):
            self.reverse[value] = index

    def __contains__(self, value):
        """
        Take advantage of the hash map for reverse index lookups to implement a
        fast membership test.
        """
        return value in self.reverse

