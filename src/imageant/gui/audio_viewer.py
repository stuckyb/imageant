# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import soundfile
import os.path
import time
import numpy as np
from PyQt5.QtWidgets import (
    QWidget, QVBoxLayout, QHBoxLayout, QSplitter, QSlider, QPushButton,
    QShortcut
)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QKeySequence
from .mobj_viewer_abc import MediaObjectViewer
from ..audio.audio_player import AudioPlayer
from .audio_widgets import WaveformView, SpectrogramView
from ..audio.au_utils import convert_to_mono


class AudioViewer(QWidget, MediaObjectViewer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        vbox = QVBoxLayout()

        audio_w_split = QSplitter()
        audio_w_split.setOrientation(Qt.Vertical)

        self.specview = SpectrogramView()
        audio_w_split.addWidget(self.specview)
        audio_w_split.setStretchFactor(0, 7)

        self.wfview = WaveformView()
        audio_w_split.addWidget(self.wfview)
        audio_w_split.setStretchFactor(1, 3)

        vbox.addWidget(audio_w_split)

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setRange(0, 0)
        # Draw tick marks at 1-second intervals.
        self.slider.setTickPosition(QSlider.TicksAbove)
        self.slider.setTickInterval(1000)
        self.slider.setEnabled(False)
        self.slider.setTracking(False)
        self.slider.valueChanged.connect(self._sliderValChanged)
        self.slider.sliderPressed.connect(self._sliderPressed)
        self.slider.sliderReleased.connect(self._sliderReleased)
        vbox.addWidget(self.slider)
        self.slider_pressed = False

        ctl_hbox = QHBoxLayout()

        self.play_icon = QIcon.fromTheme('media-playback-start')
        self.pause_icon = QIcon.fromTheme('media-playback-pause')

        self.play_button = QPushButton(self.play_icon, '')
        self.play_button.setToolTip('Play')
        self.play_button.clicked.connect(self._playClicked)
        self.play_button.setEnabled(False)
        self.play_button.setShortcut(QKeySequence(Qt.Key_Space))
        ctl_hbox.addWidget(self.play_button)

        # Disable the play/pause button from receiving focus.  This prevents
        # conflict between the spacebar shortcut for play/pause and the
        # spacebar shortcut for a button press when a button has focus.
        self.play_button.setFocusPolicy(Qt.NoFocus)

        # Set up keyboard shortcuts for rewinding and fast-forwarding the audio
        # stream position.
        left_arrow_sc = QShortcut(QKeySequence(Qt.Key_Left), self)
        left_arrow_sc.activated.connect(
            lambda: self.mediaNavKeyPressed(Qt.Key_Left)
        )
        right_arrow_sc = QShortcut(QKeySequence(Qt.Key_Right), self)
        right_arrow_sc.activated.connect(
            lambda: self.mediaNavKeyPressed(Qt.Key_Right)
        )
        right_arrow_sc = QShortcut(QKeySequence(Qt.Key_Home), self)
        right_arrow_sc.activated.connect(
            lambda: self.mediaNavKeyPressed(Qt.Key_Home)
        )

        self.stop_button = QPushButton(
            QIcon.fromTheme('media-playback-stop'), ''
        )
        self.stop_button.setToolTip('Stop')
        self.stop_button.clicked.connect(self._stopClicked)
        self.stop_button.setEnabled(False)
        ctl_hbox.addWidget(self.stop_button)
        ctl_hbox.addStretch()

        vbox.addLayout(ctl_hbox)

        self.setLayout(vbox)

        self.player = AudioPlayer(self)
        self.player.audioChange.registerObserver(self._audioChanged)
        self.player.playEvent.registerObserver(self._playEvent)
        self.player.pauseEvent.registerObserver(self._pauseEvent)
        self.player.stopEvent.registerObserver(self._stopEvent)
        self.player.positionChange.registerObserver(self._positionChanged)
        self.player.audioError.registerObserver(self._audioError)

        # Tracks the time that an audio file was displayed.
        self.audio_start_time = None

    def _sliderPressed(self):
        self.slider_pressed = True

    def _sliderReleased(self):
        self.slider_pressed = False

    def getKeyboardShortcutsInfo(self):
        return ((
            ('space', 'play/pause'),
            ('left/right', 'rewind/fast-forward 1 sec.'),
            ('home', 'rewind to beginning')
        ),)

    def mediaNavKeyPressed(self, key_const):
        """
        Responds to key presses for rewinding or fast-forwarding the audio
        stream position.
        """
        pos_ms = self.player.getPosition()

        if key_const == Qt.Key_Left:
            pos_ms -= 1000
        elif key_const == Qt.Key_Right:
            pos_ms += 1000
        elif key_const == Qt.Key_Home:
            pos_ms = 0

        if pos_ms > self.player.getDuration():
            pos_ms = self.player.getDuration()
        elif pos_ms < 0:
            pos_ms = 0

        self.player.setPosition(pos_ms)

    def zoomable(self):
        return False

    def loadMediaObject(self, audio_file):
        audio_file = os.path.abspath(audio_file)

        self.a_data, self.samp_rate = soundfile.read(
            audio_file, dtype='float32'
        )
        self.a_data = convert_to_mono(self.a_data)

        # Remove any DC offset.
        self.a_data -= np.mean(self.a_data, dtype=np.float64)

        #self.a_data, self.samp_rate = librosa.core.load(
        #    audio_file, sr=None, mono=True, duration=30
        #)
        #print(self.a_data.shape)
        #print(self.a_data.strides)
        #print('SAMP RATE:', self.samp_rate)
        #print(self.a_data.min(), self.a_data.max())
        self.wfview.setAudioData(self.a_data)
        self.specview.setAudioData(self.a_data)

        self.player.setAudio(self.a_data, self.samp_rate)

        self.audio_start_time = time.monotonic()

    def unloadMediaObject(self):
        self.a_data = None
        self.audio_start_time = None
        self.wfview.setAudioData(None)
        self.specview.setAudioData(None)
        self.player.setAudio(None, None)

    def getMediaObjTime(self):
        """
        Returns the total time in seconds that the current audio file has been
        displayed.
        """
        if self.audio_start_time is None:
            return None
        else:
            return (time.monotonic() - self.audio_start_time)

    #
    # Widget signal handlers.
    #
    def _playClicked(self):
        if self.player.getState() == AudioPlayer.PLAYING:
            self.player.pause()
        elif (
            self.player.getState() in (AudioPlayer.PAUSED, AudioPlayer.STOPPED)
        ):
            self.player.play()

    def _stopClicked(self):
        self.player.stop()

    def _positionChanged(self, pos_ms):
        # Do not programmatically change the slider position if the user is
        # interacting with the slider.
        if self.slider_pressed:
            return
        else:
            # Temporarily disconnect the signal handler so we don't respond to
            # programmatic UI updates by trying to set the audio position.
            self.slider.valueChanged.disconnect(self._sliderValChanged)
            self.slider.setValue(pos_ms)
            self.slider.valueChanged.connect(self._sliderValChanged)

    def _audioError(self, error_msg):
        raise Exception(error_msg)

    def _audioChanged(self):
        if self.player.getAudioData() is not None:
            self.play_button.setEnabled(True)
            self.slider.setEnabled(True)
            self.slider.setMaximum(self.player.getDuration())
        else:
            self.play_button.setEnabled(False)
            self.stop_button.setEnabled(False)
            self.slider.setMaximum(0)
            self.slider.setEnabled(False)

    def _playEvent(self):
        self.play_button.setIcon(self.pause_icon)
        self.play_button.setToolTip('Pause')
        self.stop_button.setEnabled(True)

    def _pauseEvent(self):
        self.play_button.setIcon(self.play_icon)
        self.play_button.setToolTip('Play')

    def _stopEvent(self):
        self.play_button.setIcon(self.play_icon)
        self.play_button.setToolTip('Play')
        self.stop_button.setEnabled(False)

    def _sliderValChanged(self, value):
        self.player.setPosition(value)

    #
    # Other "private" methods.
    #

