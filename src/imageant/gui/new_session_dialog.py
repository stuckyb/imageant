# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Defines a few custom widgets used by the main GUI.
#

import os.path
from PyQt5.QtWidgets import (
    QLabel, QFrame, QMessageBox, QSpacerItem, QSizePolicy, QDialog, QLineEdit,
    QPushButton, QDialogButtonBox, QHBoxLayout, QVBoxLayout, QFileDialog,
    QDockWidget, QGridLayout
)
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QGuiApplication, QPalette, QIcon
from ..annot_session import AnnotationSession as AS


class NewSessionDialog(QDialog):
    """
    Implements a dialog for creating a new annotation session.
    """
    def __init__(self, media_type, parent=None):
        super().__init__(parent)

        if media_type == AS.MEDIA_IMAGES:
            media_str = 'image'
        elif media_type == AS.MEDIA_AUDIO:
            media_str = 'audio'
        else:
            raise Exception('Unknown media type.')
        self.media_str = media_str

        self.setModal(True)
        self.setWindowTitle('New {0} annotation session'.format(media_str))

        msg_label = QLabel(
            '<b>{0} annotation session settings:</b>'.format(media_str.title())
        )
        msg_label.setWordWrap(True)

        v_layout = QVBoxLayout()
        v_layout.addStretch()
        v_layout.addWidget(msg_label)
        v_layout.addSpacing(8)

        g_layout = QGridLayout()
        g_layout.setRowMinimumHeight(1, 12)

        # Session file chooser.
        self.sess_path_disp = self._getPathDisp()
        f_button = QPushButton(
            QIcon.fromTheme('document-open'), ' Choose &file...', self
        )
        f_button.clicked.connect(self._chooseSessFileClicked)

        g_layout.addWidget(
            self._getBuddyLabel('Session file &name:', self.sess_path_disp),
            0, 0
        )
        g_layout.addWidget(self.sess_path_disp, 0, 1)
        g_layout.addWidget(f_button, 0, 2)

        # Script file chooser.
        self.script_path_disp = self._getPathDisp()
        f_button = QPushButton(
            QIcon.fromTheme('document-open-script'), ' Choose s&cript...', self
        )
        #f_button.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        f_button.clicked.connect(self._chooseScriptClicked)

        g_layout.addWidget(
            self._getBuddyLabel('Annotation &script:', self.script_path_disp),
            2, 0
        )
        g_layout.addWidget(self.script_path_disp, 2, 1)
        g_layout.addWidget(f_button, 2, 2)

        # Media folder chooser.
        self.media_folder_disp = self._getPathDisp()
        f_button = QPushButton(
            QIcon.fromTheme('document-open-imagefolder'), ' Choose f&older...',
            self
        )
        #f_button.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        f_button.clicked.connect(self._chooseMediaFolderClicked)

        g_layout.addWidget(
            self._getBuddyLabel(
                '{0} fo&lder:'.format(media_str.title()),
                self.media_folder_disp
            ), 3, 0
        )
        g_layout.addWidget(self.media_folder_disp, 3, 1)
        g_layout.addWidget(f_button, 3, 2)

        v_layout.addLayout(g_layout)

        buttonbox = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        )
        buttonbox.accepted.connect(self.accept)
        self.ok_button = buttonbox.button(QDialogButtonBox.Ok)
        self.ok_button.setEnabled(False)
        buttonbox.rejected.connect(self.reject)

        v_layout.addSpacing(18)
        v_layout.addWidget(buttonbox)
        v_layout.addStretch()

        self.setLayout(v_layout)

    def initFilePaths(self, sess_path, script_path, media_folder):
        if sess_path is not None:
            self.sess_path_disp.setText(sess_path)
        if script_path is not None:
            self.script_path_disp.setText(script_path)
        if media_folder is not None:
            self.media_folder_disp.setText(media_folder)

    def _getPathDisp(self, init_path=''):
        """
        Creates and returns a QLineEdit widget for displaying file paths.
        """
        path_disp = QLineEdit(init_path, self)

        # Make the path display at least ~20 ems wide in the default font.
        min_r = path_disp.fontMetrics().boundingRect('M' * 20)
        margins = path_disp.textMargins()
        path_disp.setMinimumWidth(
            min_r.width() + margins.left() + margins.right()
        )

        path_disp.textChanged.connect(self._pathEdited)

        return path_disp

    def _pathEdited(self, text):
        non_empty = (
            self.sess_path_disp.text().strip() != '' and
            self.script_path_disp.text().strip() != '' and
            self.media_folder_disp.text().strip() != ''
        )

        self.ok_button.setEnabled(non_empty)

    def _getBuddyLabel(self, label_text, path_disp):
        label = QLabel(label_text)
        label.setBuddy(path_disp)

        return label

    def _chooseSessFileClicked(self):
        fpath = QFileDialog.getSaveFileName(
            self, 'Choose an annotation session file name',
            self.sess_path_disp.text(),
            'Annotation session files (*.ia_sess);;All files(*)', None,
            QFileDialog.DontConfirmOverwrite
        )[0]

        if fpath != '':
            self.sess_path_disp.setText(fpath)

    def _chooseScriptClicked(self):
        fpath = QFileDialog.getOpenFileName(
            self, 'Choose an annotation script file',
            self.script_path_disp.text(),
            'Annotation script files (*.ias);;All files(*)'
        )[0]

        if fpath != '':
            self.script_path_disp.setText(fpath)

    def _chooseMediaFolderClicked(self):
        fpath = QFileDialog.getExistingDirectory(
            self, 'Choose an {0} folder'.format(self.media_str),
            self.media_folder_disp.text()
        )

        if fpath != '':
            self.media_folder_disp.setText(fpath)

    def _isValidFilePath(self, fpath_box, desc_str, must_exist, req_ext=None):
        """
        Validates a file path string in a text box.  Returns a tuple,
        (IS_VALID, message), where IS_VALID is a boolean value that indicates
        if the path is valid, and, if the path is not valid, message is a text
        message indicating why the path is invalid.
        """
        fpath = fpath_box.text().strip()

        if fpath == '':
            msg = (
                'The {0} file path is missing.'.format(desc_str)
            )
            return (False, msg)

        if req_ext is not None:
            if not(fpath.endswith(req_ext)):
                fpath += req_ext

        fpath = os.path.abspath(fpath)

        if not(os.path.isdir(os.path.dirname(fpath))):
            msg = (
                'The {0} file path "{1}" is not valid because one or more '
                'of the folders in the path does not '
                'exist.'.format(desc_str, fpath)
            )
            return (False, msg)

        if os.path.isdir(fpath):
            msg = (
                'The {0} file path "{1}" is not valid because a folder with '
                'the same name already exists.'.format(desc_str, fpath)
            )
            return (False, msg)

        if must_exist:
            if not(os.path.exists(fpath)):
                msg = (
                    'The {0} file "{1}" does not '
                    'exist.'.format(desc_str, fpath)
                )
                return (False, msg)

        return (True, '')

    def _isValidDirPath(self, fpath_box, desc_str, must_exist):
        """
        Validates a directory path string in a text box.  Returns a tuple,
        (IS_VALID, message), where IS_VALID is a boolean value that indicates
        if the path is valid, and, if the path is not valid, message is a text
        message indicating why the path is invalid.
        """
        fpath = fpath_box.text().strip()

        if fpath == '':
            msg = (
                'The {0} path is missing.'.format(desc_str)
            )
            return (False, msg)

        fpath = os.path.abspath(fpath)

        if not(os.path.isdir(fpath)):
            if not(os.path.exists(fpath)):
                if must_exist:
                    msg = ( 
                        'The {0} "{1}" does not exist.'.format(desc_str, fpath)
                    )
                    return (False, msg)
            else:
                msg = ( 
                    'The {0} "{1}" is not a valid '
                    'folder.'.format(desc_str, fpath)
                )
                return (False, msg)

        return (True, '')

    def accept(self):
        isvalid = self._isValidFilePath(
            self.sess_path_disp, 'annotation session', False, '.ia_sess'
        )
        if isvalid[0]:
            isvalid = self._isValidFilePath(
                self.script_path_disp, 'annotation script', True
            )
        if isvalid[0]:
            isvalid = self._isValidDirPath(
                self.media_folder_disp, 'image folder', True
            )

        if not(isvalid[0]):
            msgbox = SmartMsgBox(
                QMessageBox.Warning, '', 'File path error.',
                QMessageBox.Ok, self
            )
            msgbox.setInformativeText(isvalid[1])
            msgbox.exec()
        else:
            sess_path = self.getSessionFilePath()
            if os.path.exists(sess_path):
                resp = QMessageBox.question(
                    self, '', 'The session file "{0}" already exists.  If you '
                    'use it as the new annotation session file, any existing '
                    'session contents will be overwritten.  Do you want to '
                    'continue?'.format(os.path.basename(sess_path))
                )
                if resp == QMessageBox.Yes:
                    super().accept()
            else:
                super().accept()

    def getSessionFilePath(self):
        sess_path = self.sess_path_disp.text().strip()
        if not(sess_path.endswith('.ia_sess')):
            sess_path += '.ia_sess'

        return sess_path

    def getScriptFilePath(self):
        return os.path.abspath(self.script_path_disp.text().strip())

    def getMediaFolderPath(self):
        return os.path.abspath(self.media_folder_disp.text().strip())

