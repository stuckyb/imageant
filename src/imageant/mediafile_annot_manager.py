# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import os.path
import csv
from imageant.bidirectional_list import BidirectionalList as bilist
from imageant.actions_logger import AnnotationActionsLogger


class MediaFileAnnotationsManager:
    """
    A class that manages annotations on a set of media files, including
    progress checkpointing and output.
    """
    def __init__(
        self, media_dir, output_file, log_file, restore_state=True,
        file_exts=None
    ):
        """
        media_dir (str): The path of a directory containing media files.
        output_file (str): The path of the CSV annotations file.
        log_file (str): The path of the log file.
        restore_state (bool): If True, the state of the
            MediaFileAnnotationsManager will be restored from the log file.
        file_exts (list[str]): A list of file extensions by which to filter the
            directory contents.  Extension matching is not case-sensitive.  If
            None, all files in the directory will be used.
        """
        self.outfile = None
        self.logger = None

        # The media files folder and the list of extensions used to load the
        # media files.
        self.media_dir = ''
        self.file_exts = file_exts

        # The bilist of media files and the current media file.
        self.media_files = bilist()
        self.cur_mfile = None

        # A map for all completed media file annotations.
        self.annotations = {}

        # A map for cumulative media file times.
        self.mfile_times = {}

        # The set of annotation variables.
        self.annot_varnames = None

        # A flag to indicate whether we are in the process of restoring state.
        self.replaying_log = False

        self._loadMediaFolder(media_dir, file_exts)
        self._setOutputFiles(output_file, log_file, restore_state)

    def _initMembers(self):
        """
        Initializes all member objects to an empty state.
        """
    def _loadMediaFolder(self, dirpath, file_exts):
        """
        dirpath (str): The path of a directory containing media files.
        file_exts (list[str]): A list of file extensions by which to filter the
            directory contents.  Extension matching is not case-sensitive.  If
            None, all files in the directory will be used.
        """
        self.media_files = bilist()

        if file_exts is not None:
            file_exts = [file_ext.lower() for file_ext in file_exts]

        for fname in os.listdir(dirpath):
            fpath = os.path.join(dirpath, fname)
            if os.path.isfile(fpath):
                if file_exts is not None:
                    if os.path.splitext(fpath)[1].lower() in file_exts:
                        self.media_files.append(fname)
                else:
                    self.media_files.append(fname)

        self.media_files.sort(key=str.lower)

        if len(self.media_files) == 0:
            raise Exception(
                'No media files were found in the folder '
                '"{0}".'.format(dirpath)
            )

        self.media_dir = dirpath
        self.cur_mfile = self.media_files[0]

    def _setOutputFiles(self, output_file, log_file, restore_state=True):
        """
        Sets the main output file and logging output file.

        restore_state (bool): If True, the state of the
            MediaFileAnnotationsManager will be restored from the log file.
        """
        if os.path.exists(output_file) and not(os.path.isfile(output_file)):
            raise Exception(
                'The output file name "{0}" is already in use by another file '
                'system object.'.format(output_file)
            )

        self.outfile = output_file
        # If the output file does not exist, create it so the user can find it
        # next time.
        if not(os.path.exists(output_file)):
            with open(output_file, 'w'):
                pass

        if os.path.exists(log_file) and not(os.path.isfile(log_file)):
            raise Exception(
                'The log file name "{0}" is already in use by another file '
                'system object.'.format(log_file)
            )

        if os.path.isfile(log_file) and os.path.getsize(log_file) > 0:
            if restore_state:
                with AnnotationActionsLogger(log_file, 'r') as logger:
                    self._replayLog(logger)
            else:
                # Make a copy of the old log so we don't just lose it.
                cnt = 1
                copy_lfname = log_file
                while (
                    os.path.exists(copy_lfname) and
                    os.path.getsize(copy_lfname) > 0
                ):
                    copy_lfname = '{0}.bak-{1}'.format(log_file, cnt)
                    cnt += 1

                os.rename(log_file, copy_lfname)

        if restore_state:
            self.logger = AnnotationActionsLogger(log_file, 'a')
        else:
            self.logger = AnnotationActionsLogger(log_file, 'w')

    def __del__(self):
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    def close(self):
        # Of the resources managed by MediaFileAnnotationsManager, only the log
        # file is open indefinitely.  The output CSV file is only open within
        # writeAnnotations().
        if self.logger is not None:
            self.logger.close()

    def getOutputFile(self):
        """
        Returns the path of the output file or None if none is set.
        """
        if self.outfile is not None:
            return os.path.abspath(self.outfile)
        else:
            return None

    def getLogFile(self):
        """
        Returns the path of the log file or None if none is set.
        """
        if self.logger is not None:
            return self.logger.getLogFile()
        else:
            return None

    def getMediaFolder(self):
        return self.media_dir

    def getFileExts(self):
        return self.file_exts

    def getMediaFileCount(self):
        return len(self.media_files)

    def getMediaFileNames(self):
        return list(self.media_files)

    def getAnnotatedMediaFileCount(self):
        return len(self.annotations)

    def getActiveMediaFile(self):
        """
        Returns the file name of the active media file.
        """
        return self.cur_mfile

    def getMediaFilePath(self, media_fname):
        """
        Returns the complete path for a target media file.
        """
        if media_fname not in self.media_files:
            raise ValueError('Invalid media file name.')

        return os.path.abspath(
            os.path.join(self.media_dir, media_fname)
        )

    def _updateTime(self, mfile, time):
        """
        Updates the cumulative time for the provided media file.
        """
        if mfile not in self.mfile_times:
            self.mfile_times[mfile] = 0.0

        self.mfile_times[mfile] += time

    def getMediaFileTime(self, media_fname):
        if media_fname not in self.media_files:
            raise ValueError(
                'The media file name "{0}" is not part of the current media '
                'file set.'.format(media_fname)
            )

        if media_fname not in self.mfile_times:
            return 0.0
        else:
            return self.mfile_times[media_fname]

    def _getNextMediaFile(self):
        """
        Finds the next un-annotated media file in the media file set, or
        returns None if all media files are already annotated.
        """
        if len(self.annotations) == len(self.media_files):
            next_mf = None
        else:
            # Get the current media file index.
            cur_mf_i = self.media_files.r(self.cur_mfile)

            # Move through the media files bilist until we find the next
            # un-annotated image.
            next_mf_i = (cur_mf_i + 1) % len(self.media_files)
            while (
                next_mf_i != cur_mf_i and
                self.media_files[next_mf_i] in self.annotations
            ):
                next_mf_i = (next_mf_i + 1) % len(self.media_files)

            # If we arrived back at the current media file and it is already
            # annotated, there must be an inconsistency in the internal data
            # structures, because there are no more unclassified media files
            # but the lengths of the media_files and annotations data
            # structures do not match.
            if (
                next_mf_i == cur_mf_i and
                self.media_files[next_mf_i] in self.annotations
            ):
                raise Exception(
                    'Inconsistency detected in annotations manager: No '
                    'un-annotated media files could be found, but the number '
                    'of media file annotations was invalid.'
                )

            next_mf = self.media_files[next_mf_i]

        return next_mf

    def _replayLog(self, logger):
        """
        Restores the state of the AnnotationsManager from a log file.
        """
        try:
            self.replaying_log = True

            result = logger.readAction()
            while result is not None:
                mfile, action, time = result
                if mfile not in self.media_files:
                    raise Exception(
                        'The annotations log file references a media file that '
                        'is not part of the current media file set: '
                        '"{0}".'.format(mfile)
                    )

                if isinstance(action, int):
                    if action == AnnotationActionsLogger.A_SKIP:
                        self.cur_mfile = mfile
                        self.skipActiveMediaFile(time)
                    elif action == AnnotationActionsLogger.A_RESET:
                        self.resetMediaFile(mfile, 0.0)
                    elif action == AnnotationActionsLogger.A_TIME:
                        self.cur_mfile = mfile
                        self.updateMediaFileTime(time)
                    else:
                        raise Exception(
                            'Unknown annotation action recorded in log file: '
                            '{0}.'.format(action)
                        )
                elif isinstance(action, dict):
                    self.cur_mfile = mfile
                    self.setAnnotations(action, time)
                else:
                    raise Exception(
                        'Unrecognized annotation action in log file.'
                    )

                result = logger.readAction()
        finally:
            self.replaying_log = False

    def _logAction(self, mfile, action, time):
        if mfile is None:
            return

        # Always update the image time, even if we are replaying a log file.
        if time > 0.0:
            self._updateTime(mfile, time)

        if not(self.replaying_log):
            self.logger.logAction(mfile, action, time)

    def setAnnotations(self, annotations, time):
        """
        annotations: A dictionary of annotations for the active image.
        time: The time interval to log for the annotations.
        """
        if self.cur_mfile is None:
            raise Exception(
                'Attempted to run an annotation action but there is no active, '
                'un-annotated media file in the current media file set.'
            )

        if self.annot_varnames is None:
            self.annot_varnames = set(annotations.keys())

        if self.annot_varnames != annotations.keys():
            raise Exception(
                'The variable names in the new set of annotations do not match '
                'the variable names of previous annotations.'
            )

        self.annotations[self.cur_mfile] = annotations
        self._logAction(self.cur_mfile, annotations, time)
        self.cur_mfile = self._getNextMediaFile()

    def skipActiveMediaFile(self, time):
        """
        Move the active media file to the next un-annotated media file.

        time: The time interval to log for the skip action.
        """
        if self.cur_mfile is None:
            raise Exception(
                'Attempted to run an annotation action but there is no active, '
                'un-annotated media file in the current media file set.'
            )

        self._logAction(self.cur_mfile, AnnotationActionsLogger.A_SKIP, time)
        self.cur_mfile = self._getNextMediaFile()

    def resetMediaFile(self, media_fname, time):
        """
        Removes annotations for the specified media file, if any, and makes
        that media file the active media file.  Two actions are logged: First,
        a time interval for the length of time that the current media file was
        displayed before it was replaced with the reset media file, and second,
        an action recording the annotations reset.

        time: The time interval to log for the media file that was replaced by
            the reset media file.
        """
        if media_fname in self.annotations:
            del self.annotations[media_fname]

        self.updateMediaFileTime(time)
        self._logAction(media_fname, AnnotationActionsLogger.A_RESET, 0.0)
        self.cur_mfile = media_fname

    def updateMediaFileTime(self, time):
        """
        Updates the time interval for the current media file without modifying
        its annotations or logging any other action.
        """
        self._logAction(self.cur_mfile, AnnotationActionsLogger.A_TIME, time)

    def getAnnotations(self):
        return self.annotations.copy()

    def writeAnnotations(self):
        if self.outfile is None:
            raise Exception('No output file was provided.')

        if self.annot_varnames is None:
            raise Exception('No annotations have been completed.')

        with open(self.outfile, 'w', newline='') as fout:
            writer = csv.DictWriter(
                fout, ['file'] + list(self.annot_varnames) + ['time']
            )
            writer.writeheader()

            for mfile in self.media_files:
                if mfile in self.annotations:
                    rowout = self.annotations[mfile].copy()
                    rowout['file'] = mfile
                    rowout['time'] = self.getMediaFileTime(mfile)
                    writer.writerow(rowout)

