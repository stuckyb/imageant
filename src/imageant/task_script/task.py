# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class TaskResponse:
    # An ID for the "skip" response that will never be used by user-defined
    # responses.  Note that this response should NOT be passed to the script
    # interpreter, because it will not be recognized.
    RESPONSE_SKIP = -1

    # Define the allowable response types.
    KEYPRESS = 0

    def __init__(
        self, response_id, response_type, text, jump_to=None, key_val=None,
        response_val=None
    ):
        """
        response_id: An integer response identifier (only unique within the
            task).
        response_type: The type of response to request from the user.
        text: The response text to display.
        jump_to: The name of a task list, or None.
        key_val: A key to press.  Only used for KEYPRESS responses.
        response_val: The response value for a key press.  Only used for
            KEYPRESS responses.
        """
        if response_type not in (TaskResponse.KEYPRESS,):
            raise Exception(
                'Invalid task response type: {0}.'.format(response_type)
            )

        self.id = response_id
        self.response_type = response_type
        self.text = text
        self.jump_to = jump_to
        self.key_val = key_val
        self.response_val = response_val

    def __eq__(self, other):
        if other is None:
            return False
        else:
            return (
                self.id == other.id and
                self.response_type == other.response_type and
                self.text == other.text and
                self.jump_to == other.jump_to and
                self.key_val == other.key_val and
                self.response_val == other.response_val
            )            


class AnnotationTask:
    def __init__(self, text, varname, default):
        """
        text: The task text to display.
        varname: The variable name in which to record task responses.
        default: Default value to use if this task is dependent on a parent
            annotation task.
        """
        self.text = text
        self.varname = varname
        self.default = default
        self.responses = []
        self.dependents = []

    def __eq__(self, other):
        if other is None:
            return False
        else:
            return (
                self.text == other.text and
                self.varname == other.varname and
                self.default == other.default and
                self.responses == other.responses and
                self.dependents == other.dependents
            )

    def addResponse(
        self, response_type, text='', jump_to=None, key_val=None,
        response_val=None
    ):
        """
        response_type: The type of response to request from the user.
        text: The response text to display.
        jump_to: The name of a task list, or None.
        key_val: A key to press.  Only used for KEYPRESS responses.
        response_val: The response value for a key press.  Only used for
            KEYPRESS responses.
        """
        # Use the response list index as the response ID.
        response = TaskResponse(
            len(self.responses), TaskResponse.KEYPRESS, text, jump_to, key_val,
            response_val
        )
        self.responses.append(response)

        if jump_to is not None:
            self.dependents.append(jump_to)

