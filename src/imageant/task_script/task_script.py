# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class TaskScript:
    """
    A class that defines a complete annotation task script.  Tasks list within
    the script can be accessed using subscript notation with either an integer
    index or a task list name string.  Task list indices correspond with the
    order in which they are created.
    """
    def __init__(self):
        # A list to keep track of task list insertion order and to support
        # retrieving lists by index.
        self._listorder = []

        # A dictionary that maps task list names to task lists.
        self._tlists = {}

    def addTaskList(self, list_name):
        """
        Adds a new named task list to this TaskScript and returns a reference
        to the list.
        """
        list_name = str(list_name)

        if list_name in self._tlists:
            raise ValueError(
                'Unable to create the new task list because a task list with '
                'the name "{0}" already exists.  All task list names must be '
                'unique.'.format(list_name)
            )

        self._tlists[list_name] = []
        self._listorder.append(list_name)

        return self._tlists[list_name]

    def getTaskListCount(self):
        """
        Returns the number of task lists in this script.
        """
        return len(self)

    def getTaskListName(self, index):
        """
        Returns the name of the task list at the given index.
        """
        try:
            list_name = self._listorder[index]
        except IndexError:
            raise IndexError('Invalid task list index: {0}.'.format(index))

        return list_name

    def __len__(self):
        """
        Returns the number of task lists in this script.
        """
        return len(self._listorder)

    def __contains__(self, listname):
        """
        Tests whether the given listname is valid for this script.
        """
        return listname in self._tlists

    def __getitem__(self, key):
        """
        Supports using subscript notation to access task lists by either
        integer index (according to insertion order) or name strings.
        """
        if isinstance(key, int):
            try:
                listref = self._tlists[self._listorder[key]]
            except IndexError:
                raise IndexError('Invalid task list index: {0}.'.format(key))
        elif isinstance(key, str):
            if key not in self._tlists:
                raise KeyError('Invalid task list name: "{0}".'.format(key))

            listref = self._tlists[key]
        else:
            raise IndexError(
                'Invalid type for accessing task lists: {0}.'.format(type(key))
            )

        return listref

