# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class TaskUndoStack(list):
    """
    Implements a specialized stack for tracking task undo operations.
    """
    def push(self, obj_id, interp_state):
        """
        obj_id: The ID of a media object associated with the given state.
        interp_state: A memento capturing the script interpreter's state.
        """
        self.append((obj_id, interp_state))

    def hasRewindableLastObject(self, cur_obj):
        """
        Returns True if there are annotations operations on the stack for a
        previous media object, False otherwise.

        cur_obj: The ID of the currently active media object.
        """
        # Skip any operations on the current media object.
        i = len(self) - 1
        while i >= 0 and self[i][0] == cur_obj:
            i -= 1

        return (i >= 0 and self[i][0] != cur_obj)

    def rewindLastMediaObject(self, cur_obj):
        """
        Removes undoable operations until the state at the beginning of the
        previous media object is reached, and then returns this state.  If
        there is no previous media object on the stack, returns (None, None)
        without changing the state of the stack.

        cur_obj: The ID of the currently active media object.
        """
        if not(self.hasRewindableLastObject(cur_obj)):
            return (None, None)

        # Remove any operations on the current media object.
        while len(self) > 0 and self[-1][0] == cur_obj:
            self.pop()

        # Rewind to the beginning of the previous media object.
        if len(self) > 0:
            last_obj = self[-1][0]
            while len(self) > 0 and self[-1][0] == last_obj:
                op = self.pop()

            return op
        else:
            raise Exception(
                'Inconsistent undo operations history detected.  No previous '
                'media object operations were found.'
            )

