# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
from argparse import ArgumentParser
import sys
from PyQt5.QtWidgets import QApplication
from imageant.gui import ImageAntUI


argp = ArgumentParser(
    prog='imageant', description='Software for efficient, flexible media file '
    'annotation.'
)
argp.add_argument(
    '-s', '--session_file', type=str, required=False, default='', help='The '
    'path to an annotation session file.  If this option is provided, '
    '--session_name, --script, and --media_dir will all be ignored.'
)
argp.add_argument(
    '-n', '--session_name', type=str, required=False, default='', help='A name '
    'to use for a new annotation session.  If this option is provided, '
    '--script and --media_dir must also be provided.'
)
argp.add_argument(
    '-t', '--script', type=str, required=False, default='', help='The path to '
    'an annotation script.'
)
argp.add_argument(
    '-d', '--media_dir', type=str, required=False, default='', help='The path '
    'to a folder containing media files to annotate.'
)
argp.add_argument(
    '-m', '--media_type', type=str, choices=('image', 'audio'), required=False,
    default='image', help='The type of media file to annotate.  Must be either '
    '"image" or "audio" (default: "image").'
)

args = argp.parse_args()

if args.session_name != '' and (args.script == '' or args.imgdir == ''):
    exit(
        '\nERROR: If -n/--session_name is provided, -t/--script and '
        '-d/--media_dir must also be provided.\n'
    )

app = QApplication(sys.argv)
app.setOrganizationName('ImageAnt')
app.setApplicationName('ImageAnt')

ui = ImageAntUI()
ui.show()

if args.session_file != '':
    ui.restoreSession(args.session_file)
elif args.session_name != '' and (args.script != '' and args.imgdir != ''):
    sess_name = args.session_name
    if not(sess_name.endswith('.ia_sess')):
        sess_name += '.ia_sess'

    if args.media_type == 'image':
        mtype = AnnotationSession.MEDIA_IMAGES
    elif args.media_type == 'audio':
        mtype = AnnotationSession.MEDIA_AUDIO
    else:
        exit('\nERROR: Invalid media type: "{0}".\n'.format(args.media_type))

    ui.newSession(sess_name, args.script, args.imgdir)

sys.exit(app.exec_())

