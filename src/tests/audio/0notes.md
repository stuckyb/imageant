
This folder contains audio files for audio-related unit tests.  Descriptions of the files follow.

* stereo_to_mono.wav: For testing stereo-to-mono conversion.  The left channel contains 1 second of samples at a constant value of 0.1 followed by 1 second at a constant value of 0.8.  The right channel contains 1 second at a constant value of 0.3 followed by 1 second at a constant value of 0.6.

* choppedaudio_short.wav: For testing ChoppedAudio's audio splitting functionality.  Contains a single channel with 0.25 seconds of samples at a constant value of 0.1.

* chopped_audio_long.wav: For testing ChoppedAudio's audio splitting functionality.  Contains a single channel with 0.5 seconds of samples at a constant value of 0.1, 0.5 seconds of samples at a constant value of 0.2, and 0.25 seconds of samples at a constant value of 0.4.

