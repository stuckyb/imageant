#!/usr/bin/python3

# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import soundfile as sf
import numpy as np
import math
import os.path
from argparse import ArgumentParser, Action


class ShowFormatsAction(Action):
    """
    And argparse Action class that prints all available audio formats and then
    exits the main program.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        f_dict = sf.available_formats()
        for format_str in sorted(f_dict.keys()):
            print('"{0}": {1}'.format(format_str, f_dict[format_str]))
        parser.exit(0)


argp = ArgumentParser(
    description='Generates simple test audio files.'
)
argp.add_argument(
    '-f', '--freqs', type=str, required=True, help='A comma-separated list of '
    'output frequencies, one per output audio segment.  If the --constvals '
    'flag is provided, these values are interpreted as constant sample values '
    'instead of frequencies, and in that case must be in the range [-1.0, 1.0].'
)
argp.add_argument(
    '--freqs_2', type=str, required=False, default='', help='Same as --freqs, '
    'but for the second stereo audio channel.  If the output is stereo and '
    'this argument is not provided, the values from --freqs will be duplicated '
    'for the second channel.  Ignored if the output is mono or if --invert is '
    'specified.'
)
argp.add_argument(
    '-l', '--length', type=float, required=False, default=1.0,
    help='The length of each output audio segment (default: 1.0).'
)
argp.add_argument(
    '-s', '--stereo', action='store_true',
    help='Generate stereo audio instead of mono.'
)
argp.add_argument(
    '-i', '--invert', action='store_true',
    help='Make the second audio stereo channel the inverse of the first.  '
    'Ignored if the output is mono.'
)
argp.add_argument(
    '-c', '--constvals', action='store_true', help='Interpret the output '
    '"frequencies" as constant sample values instead of frequencies.'
)
argp.add_argument(
    '--format', type=str, required=False, default='WAV', help='The format of '
    'the output audio file.  To see all available audio formats, run '
    'mk_test_audio.py with the --show_formats option.  Default: "WAV".'
)
argp.add_argument(
    '--show_formats', nargs=0, action=ShowFormatsAction, help='Print '
    'information about all available output audio formats and exit.'
)
argp.add_argument(
    '--scale_fact', type=float, required=False, default=1.0,
    help='A multiplicative scaling factor to apply to all audio samples (e.g., '
    '0.75 to decrease the output amplitude).  Default=1.0.'
)
argp.add_argument(
    'output_file', type=str, help='The path of the output audio file.'
)

args = argp.parse_args()

if args.scale_fact < 0.0 or args.scale_fact > 1.0:
    exit(
        '\nERROR: Scale factors may not be less than 0.0 or greater than 1.0.\n'
    )

freqlist_c1 = [float(fstr) for fstr in args.freqs.split(',')]
if args.freqs_2 != '':
    freqlist_c2 = [float(fstr) for fstr in args.freqs_2.split(',')]
else:
    freqlist_c2 = freqlist_c1

if len(freqlist_c2) != 0 and len(freqlist_c1) != len(freqlist_c2):
    exit('\nERROR: The frequency list lengths must match.\n')

if args.constvals:
    if (
        min(freqlist_c1) < -1.0 or min(freqlist_c2) < -1.0 or
        max(freqlist_c1) > 1.0 or max(freqlist_c2) > 1.0
    ):
        exit(
            '\nERROR: Constant sample values may not be less than -1.0 or '
            'greater than 1.0.\n'
        )

if os.path.exists(args.output_file):
    exit('\nERROR: The output file path already exists.\n')

# Segment length and total length, in samples.
seg_len = round(44100 * args.length)
t_len = seg_len * len(freqlist_c1)

if args.stereo:
    samps = np.zeros((t_len, 2))
else:
    samps = np.zeros((t_len, 1))

# Calculate time values for each segment.
seg_t_vals = np.linspace(0.0, seg_len / 44100, num=seg_len, endpoint=False)

# Generate the audio data.
for cnt, freq in enumerate(freqlist_c1):
    start = cnt * seg_len
    end = (cnt + 1) * seg_len

    if args.constvals:
        samps[start:end, 0] = freq
    else:
        samps[start:end, 0] = np.sin(seg_t_vals * math.pi * 2 * freq)

    if args.stereo:
        if args.invert:
            samps[start:end, 1] = 0 - samps[start:end, 0]
        else:
            freq2 = freqlist_c2[cnt]
            if args.constvals:
                samps[start:end, 1] = freq2
            else:
                samps[start:end, 1] = np.sin(seg_t_vals * math.pi * 2 * freq2)

samps *= args.scale_fact

sf.write(args.output_file, samps, samplerate=44100, format=args.format)

