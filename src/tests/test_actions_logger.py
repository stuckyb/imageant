# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import tempfile
import os.path
import shutil
import unittest
from imageant.actions_logger import AnnotationActionsLogger


# Define log encoding test data.  The format of each tuple is
# ( encoded_string, (filename, action) ).
logenc_data = [
    ( '"",0,0.0', ('', 0, 0) ),
    ( '"img.jpg",0,1.23', ('img.jpg', 0, 1.23) ),
    (
        '"img.jpg",["",""],-1.0',
        ('img.jpg', {'var1': '', 'var2': ''}, -1)
    ),
    (
        '"img.jpg",["str1","str2"],0.01',
        ('img.jpg', {'var1': 'str1', 'var2': 'str2'}, 0.01)
    ),
    (
        r'"img.jpg",["a\nmultiline\nstring","str2"],12.04',
        ('img.jpg', {'var1': 'a\nmultiline\nstring', 'var2': 'str2'}, 12.04)
    )
]

# Actions for testing logging.
log_actions = [
    ('img0.jpg', AnnotationActionsLogger.A_SKIP, 0.0),
    ('img0.jpg', AnnotationActionsLogger.A_RESET, 0.123),
    ('img0.jpg', AnnotationActionsLogger.A_TIME, 0.321),
    ('img0.jpg', {'var1': 'var1_0', 'var2': 'var2_0'}, 1.23),
    ('img1.jpg', {'var1': 'var1_1', 'var2': 'var2_1'}, 12.3),
    ('img2.jpg', {'var1': 'var1_2', 'var2': 'var2_2'}, -1.0),
    ('img2.jpg', AnnotationActionsLogger.A_RESET, 12.04),
    ('img2.jpg', {'var1': 'var1_2_1', 'var2': 'var2_2_1'}, 1.0),
    ('img0.jpg', AnnotationActionsLogger.A_SKIP, 124.02),
    ('img4.jpg', {'var1': 'var1_4', 'var2': 'var2_4'}, 2.1),
    ('img5.jpg', {'var1': 'var1_5', 'var2': 'var2_5'}, 3.2),
    ('img3.jpg', {'var1': 'var1_3', 'var2': 'var2_3'}, 4.3)
]


class TestAnnotationActionsLogger(unittest.TestCase):
    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def _getLogger(self, mode, fname=None):
        if fname is None:
            fname = 'log'

        fpath = os.path.join(self.tmpdir, fname)

        return AnnotationActionsLogger(fpath, mode)

    def test_close(self):
        l = AnnotationActionsLogger(os.path.join(self.tmpdir, 'log'))

        l.close()

        with self.assertRaisesRegex(Exception, 'closed log file'):
            l.logAction('img0.jpg', AnnotationActionsLogger.A_SKIP, 0.0)
            
        with self.assertRaisesRegex(Exception, 'closed log file'):
            l.readAction()

    def test_logEncode(self):
        with self._getLogger('w') as l:
            l.varname_order = ['var1', 'var2']

            for encstr, orig in logenc_data:
                self.assertEqual(
                    encstr, l._logEncode(orig[0], orig[1], orig[2])
                )

            with self.assertRaisesRegex(
                Exception, 'variable name .* does not match'
            ):
                l._logEncode('filename', {'invalid': ''}, 0.0)

            with self.assertRaisesRegex(
                Exception, 'Unrecognized annotation action.'
            ):
                l._logEncode('filename', ['var1', 'val'], 0.0)

    def test_logDecode(self):
        with self._getLogger('w') as l:
            l.varname_order = ['var1', 'var2']

            for encstr, orig in logenc_data:
                fname, action, time = l._logDecode(encstr)
                self.assertEqual(orig[0], fname)
                self.assertEqual(orig[1], action)

            with self.assertRaisesRegex(
                Exception, 'Unrecognized annotation action.'
            ):
                l._logDecode('"filename",{"var1":"val"},1.0')

    def test_logging(self):
        with self._getLogger('w') as l:
            with self.assertRaisesRegex(Exception, 'not readable'):
                l.readAction()

            l = self._getLogger('r')
            self.assertIsNone(l.readAction())

            l = self._getLogger('w')
            for log_action in log_actions:
                l.logAction(log_action[0], log_action[1], log_action[2])

            del l

            l = self._getLogger('r')
            for log_action in log_actions:
                filename, action, time = l.readAction()
                self.assertEqual(filename, log_action[0])
                self.assertEqual(action, log_action[1])
                self.assertEqual(time, log_action[2])

            self.assertIsNone(l.readAction())

