# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import unittest
from imageant.annot_utils import AnnotationStats, MediaObjectAnnotationStats


class TestAnnotationStats(unittest.TestCase):
    def test_update(self):
        ast = AnnotationStats()

        with self.assertRaises(KeyError):
            ast['a']

        ast.update('a')
        self.assertEqual(1, ast['a'])

        ast.update('a')
        ast.update('a')
        self.assertEqual(3, ast['a'])

        with self.assertRaises(KeyError):
            ast['b']

        ast.update('b')
        self.assertEqual(1, ast['b'])
        self.assertEqual(3, ast['a'])

    def test_getAnnotVals(self):
        ast = AnnotationStats()

        self.assertEqual([], ast.getAnnotVals())

        annots = ['c', 'b', 'a', 'a', 'b', 'a']
        for annot in annots:
            ast.update(annot)

        self.assertEqual(['a', 'b', 'c'], ast.getAnnotVals())

    def test_stats(self):
        ast = AnnotationStats()

        # Test an empty AnnotationStats.
        self.assertEqual(0, ast.getCountTotal())
        self.assertIsNone(ast.getMajority())
        self.assertIsNone(ast.getPlurality())
        with self.assertRaises(KeyError):
            ast.getProportion('a')

        # Basic tests.
        ast.update('a')
        self.assertEqual(1, ast.getCountTotal())
        self.assertEqual(1.0, ast.getProportion('a'))
        self.assertEqual('a', ast.getMajority())
        self.assertEqual('a', ast.getPlurality())

        ast.update('a')
        ast.update('a')
        self.assertEqual(3, ast.getCountTotal())
        self.assertEqual(1.0, ast.getProportion('a'))
        self.assertEqual('a', ast.getMajority())
        self.assertEqual('a', ast.getPlurality())

        ast.update('b')
        self.assertEqual(4, ast.getCountTotal())
        self.assertEqual(0.75, ast.getProportion('a'))
        self.assertEqual(0.25, ast.getProportion('b'))
        self.assertEqual('a', ast.getMajority())
        self.assertEqual('a', ast.getPlurality())

        # Multiple values, no majority or plurality.
        ast = AnnotationStats()
        annots = ['c', 'c', 'a', 'a', 'b', 'b', 'd', 'd']
        for annot in annots:
            ast.update(annot)
        self.assertEqual(8, ast.getCountTotal())
        self.assertEqual(0.25, ast.getProportion('a'))
        self.assertEqual(0.25, ast.getProportion('b'))
        self.assertEqual(0.25, ast.getProportion('c'))
        self.assertEqual(0.25, ast.getProportion('d'))
        self.assertIsNone(ast.getMajority())
        self.assertIsNone(ast.getPlurality())

        # Multiple values, plurality but no majority.
        ast = AnnotationStats()
        annots = ['c', 'c', 'a', 'a', 'b', 'b', 'a', 'a']
        for annot in annots:
            ast.update(annot)
        self.assertEqual(8, ast.getCountTotal())
        self.assertEqual(0.5, ast.getProportion('a'))
        self.assertEqual(0.25, ast.getProportion('b'))
        self.assertEqual(0.25, ast.getProportion('c'))
        self.assertIsNone(ast.getMajority())
        self.assertEqual('a', ast.getPlurality())

        # Multiple values, plurality and majority.
        ast = AnnotationStats()
        annots = ['a', 'c', 'a', 'a', 'a', 'b', 'a', 'a']
        for annot in annots:
            ast.update(annot)
        self.assertEqual(8, ast.getCountTotal())
        self.assertEqual(0.75, ast.getProportion('a'))
        self.assertEqual(0.125, ast.getProportion('b'))
        self.assertEqual(0.125, ast.getProportion('c'))
        self.assertEqual('a', ast.getMajority())
        self.assertEqual('a', ast.getPlurality())


class TestMediaObjectAnnotationStats(unittest.TestCase):
    def test_all(self):
        """
        Basic integration tests for MediaObjectAnnotationStats.
        """
        moas = MediaObjectAnnotationStats(
            include_plurality=True, none_val="NA"
        )

        self.assertEqual([], moas.getMediaObjIds())
        self.assertEqual([], moas.getVarNames())
        with self.assertRaises(KeyError):
            moas.getAnnotations('fake_id')

        moas.update('id1', {'var1': 'a', 'var2': 1, 'time': 1.0}) 
        exp_varnames = [
            'var1-n', 'var1-majority', 'var1-plurality', 'var1-all',
            'var2-n', 'var2-majority', 'var2-plurality', 'var2-all',
            'time-n', 'time-mean', 'time-min', 'time-max', 'time-all'
        ]
        exp_annots = {
            'var1-n': 1, 'var1-majority': 'a', 'var1-plurality': 'a',
            'var1-all': '"a"(1)',
            'var2-n': 1, 'var2-majority': 1, 'var2-plurality': 1,
            'var2-all': '"1"(1)',
            'time-n': 1, 'time-mean': 1.0, 'time-min': 1.0, 'time-max': 1.0,
            'time-all': '1.0'
        }
        self.assertEqual(['id1'], moas.getMediaObjIds())
        self.assertEqual(exp_varnames, moas.getVarNames())
        self.assertEqual(exp_annots, moas.getAnnotations('id1'))

        moas.update('id1', {'var1': 'b', 'var2': 2, 'time': 2.0}) 
        exp_annots = {
            'var1-n': 2, 'var1-majority': 'NA', 'var1-plurality': 'NA',
            'var1-all': '"a"(1),"b"(1)',
            'var2-n': 2, 'var2-majority': 'NA', 'var2-plurality': 'NA',
            'var2-all': '"1"(1),"2"(1)',
            'time-n': 2, 'time-mean': 1.5, 'time-min': 1.0, 'time-max': 2.0,
            'time-all': '1.0,2.0'
        }
        self.assertEqual(['id1'], moas.getMediaObjIds())
        self.assertEqual(exp_varnames, moas.getVarNames())
        self.assertEqual(exp_annots, moas.getAnnotations('id1'))

        moas.update('id1', {'var1': 'a', 'var2': 1, 'time': 3.0}) 
        exp_annots = {
            'var1-n': 3, 'var1-majority': 'a', 'var1-plurality': 'a',
            'var1-all': '"a"(2),"b"(1)',
            'var2-n': 3, 'var2-majority': 1, 'var2-plurality': 1,
            'var2-all': '"1"(2),"2"(1)',
            'time-n': 3, 'time-mean': 2.0, 'time-min': 1.0, 'time-max': 3.0,
            'time-all': '1.0,2.0,3.0'
        }
        self.assertEqual(['id1'], moas.getMediaObjIds())
        self.assertEqual(exp_varnames, moas.getVarNames())
        self.assertEqual(exp_annots, moas.getAnnotations('id1'))

        moas.update('id2', {'var1': 'c', 'var2': 4, 'time': 10.0}) 
        exp_annots_id2 = {
            'var1-n': 1, 'var1-majority': 'c', 'var1-plurality': 'c',
            'var1-all': '"c"(1)',
            'var2-n': 1, 'var2-majority': 4, 'var2-plurality': 4,
            'var2-all': '"4"(1)',
            'time-n': 1, 'time-mean': 10.0, 'time-min': 10.0, 'time-max': 10.0,
            'time-all': '10.0'
        }
        self.assertEqual(['id1', 'id2'], moas.getMediaObjIds())
        self.assertEqual(exp_varnames, moas.getVarNames())
        self.assertEqual(exp_annots, moas.getAnnotations('id1'))
        self.assertEqual(exp_annots_id2, moas.getAnnotations('id2'))

