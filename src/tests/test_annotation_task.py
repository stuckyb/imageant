# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest
from imageant.task_script.task import AnnotationTask, TaskResponse


class TestAnnotationTask(unittest.TestCase):
    def test_addResponse(self):
        t = AnnotationTask('task1', 'taskvar', '')

        t.addResponse(TaskResponse.KEYPRESS, 'resp1', None, 'y', 'yes')
        r = t.responses[0]
        self.assertEqual(1, len(t.responses))
        self.assertEqual(
            (0, TaskResponse.KEYPRESS, 'resp1', None, 'y', 'yes'),
            (
                r.id, r.response_type, r.text, r.jump_to, r.key_val,
                r.response_val
            )
        )
        self.assertEqual([], t.dependents)

        t.addResponse(TaskResponse.KEYPRESS, 'resp2', 'list2', 'n', 'no')
        r = t.responses[1]
        self.assertEqual(2, len(t.responses))
        self.assertEqual(
            (1, TaskResponse.KEYPRESS, 'resp2', 'list2', 'n', 'no'),
            (
                r.id, r.response_type, r.text, r.jump_to, r.key_val,
                r.response_val
            )
        )
        self.assertEqual(['list2'], t.dependents)

    def test_comparison(self):
        """
        Tests comparison of tasks, which also tests comparison of responses.
        """
        t1 = AnnotationTask('task1', 'taskvar', '')
        t1.addResponse(TaskResponse.KEYPRESS, 'resp1', None, 'y', 'yes')
        t1.addResponse(TaskResponse.KEYPRESS, 'resp2', 'list2', 'n', 'no')

        # Check reference equality.
        t2 = t1
        self.assertEqual(t1, t2)

        # Check equality of distinct objects.
        t2 = AnnotationTask('task1', 'taskvar', '')
        t2.addResponse(TaskResponse.KEYPRESS, 'resp1', None, 'y', 'yes')
        t2.addResponse(TaskResponse.KEYPRESS, 'resp2', 'list2', 'n', 'no')
        self.assertEqual(t1, t2)

        # Check a few inequality cases.
        t2.varname = 'othervar'
        self.assertNotEqual(t1, t2)
        t2.varname = 'taskvar'
        self.assertEqual(t1, t2)

        t2.dependents.append('list3')
        self.assertNotEqual(t1, t2)
        t2.dependents.pop()
        self.assertEqual(t1, t2)

        t2.responses[0].key_val = 'u'
        self.assertNotEqual(t1, t2)
        t2.responses[0].key_val = 'y'
        self.assertEqual(t1, t2)

        # Check comparison to None.
        self.assertNotEqual(t1, None)
        self.assertNotEqual(t1.responses[0], None)

