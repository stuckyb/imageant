# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import random
import soundfile as sf
import numpy as np
from imageant.audio.au_utils import convert_to_mono, ChoppedAudio


class TestAudioUtilFuncs(unittest.TestCase):
    def test_convert_to_mono(self):
        a_data, samp_rate = sf.read('audio/stereo_to_mono.wav')

        # Expected sample values for the stereo-to-mono conversion
        exp = np.zeros(44100 * 2)
        exp[0:44100] = 0.2
        exp[44100:44100*2] = 0.7

        # Conversion of stereo to mono.
        self.assertEqual((44100*2, 2), a_data.shape)
        mono = convert_to_mono(a_data)
        self.assertTrue(np.allclose(exp, mono, rtol=0.0, atol=0.0001))

        # Expected sample values for the left channel only.
        exp = np.zeros(44100 * 2)
        exp[0:44100] = 0.1
        exp[44100:44100*2] = 0.8

        # Conversion of (samples x 1) audio to mono.
        audio_in = a_data[:,0].reshape((-1, 1))
        self.assertEqual((44100*2, 1), audio_in.shape)
        mono = convert_to_mono(audio_in)
        self.assertTrue(np.allclose(exp, mono, rtol=0.0, atol=0.0001))

        # Conversion of audio vector to mono.
        audio_in = a_data[:,0]
        self.assertEqual((44100*2,), audio_in.shape)
        mono = convert_to_mono(audio_in)
        self.assertTrue(np.allclose(exp, mono, rtol=0.0, atol=0.0001))

class TestChoppedAudio(unittest.TestCase):
    def test_len(self):
        # No overlap, one chunk fits perfectly.
        ac = ChoppedAudio('audio/stereo_to_mono.wav', 2.0, 0.0)
        self.assertEqual(1, len(ac))

        # No overlap, chunk size larger than audio stream size.
        ac = ChoppedAudio('audio/stereo_to_mono.wav', 2.1, 0.0)
        self.assertEqual(1, len(ac))

        # No overlap, chunk size slightly smaller than audio stream size.
        ac = ChoppedAudio('audio/stereo_to_mono.wav', 1.8, 0.0)
        self.assertEqual(2, len(ac))

        # No overlap, 2 chunks fit perfectly.
        ac = ChoppedAudio('audio/stereo_to_mono.wav', 1.0, 0.0)
        self.assertEqual(2, len(ac))

        # No overlap, 2 chunks fit with some left over.
        ac = ChoppedAudio('audio/stereo_to_mono.wav', 0.8, 0.0)
        self.assertEqual(3, len(ac))

        # 50% overlap, one chunk fits perfectly.
        ac = ChoppedAudio('audio/stereo_to_mono.wav', 2.0, 0.5)
        self.assertEqual(1, len(ac))

        # 50% overlap, one chunk fits with some left over.
        ac = ChoppedAudio('audio/stereo_to_mono.wav', 1.5, 0.5)
        self.assertEqual(2, len(ac))

        # 50% overlap, 3 chunks fit perfectly.
        ac = ChoppedAudio('audio/stereo_to_mono.wav', 1.0, 0.5)
        self.assertEqual(3, len(ac))

        # 50% overlap, 3 chunks fit with some left over.
        ac = ChoppedAudio('audio/stereo_to_mono.wav', 0.8, 0.5)
        self.assertEqual(4, len(ac))

        # Test using only part of the input data.

        # No overlap, one chunk larger than input size.
        ac = ChoppedAudio(
            'audio/stereo_to_mono.wav', 2.0, 0.0, True, input_max=1.0
        )
        self.assertEqual(1, len(ac))

        # No overlap, 1.5 chunks fit.
        ac = ChoppedAudio(
            'audio/stereo_to_mono.wav', 1.0, 0.0, True, input_max=1.5
        )
        self.assertEqual(2, len(ac))

        # No overlap, 2.0 chunks fit.
        ac = ChoppedAudio(
            'audio/stereo_to_mono.wav', 0.75, 0.0, True, input_max=1.5
        )
        self.assertEqual(2, len(ac))

        # 50% overlap, 3.0 chunks fit.
        ac = ChoppedAudio(
            'audio/stereo_to_mono.wav', 0.5, 0.5, True, input_max=1.0
        )
        self.assertEqual(3, len(ac))

    def _sequence_check(self, ac, expected):
        """
        An internal method that tests the subscript and interable interfaces of
        a ChoppedAudio instance against a list of expected values.
        """
        self.assertEqual(len(expected), len(ac))

        # Check the subscript interface.  Test the indices in random order to
        # ensure there is no sequential dependency, and check each index twice
        # to ensure repeat access works correctly.
        indices = list(range(len(expected)))
        random.shuffle(indices)
        for index in indices:
            self.assertTrue(
                np.allclose(expected[index], ac[index], rtol=0.0, atol=0.0001)
            )
            self.assertTrue(
                np.allclose(expected[index], ac[index], rtol=0.0, atol=0.0001)
            )

        for cnt, chunk in enumerate(ac):
            self.assertTrue(
                np.allclose(expected[cnt], chunk, rtol=0.0, atol=0.0001)
            )

    def test_sequence_interface(self):
        """
        Tests ChoppedAudio's sequence interface, both subscript access and
        interation.
        """
        # No overlap, one chunk fits perfectly.
        ac = ChoppedAudio('audio/choppedaudio_short.wav', 0.25, 0.0)
        exp = [np.zeros(44100 // 4)]
        exp[0] = 0.1
        self._sequence_check(ac, exp)

        # 50% overlap, one chunk fits perfectly.
        ac = ChoppedAudio('audio/choppedaudio_short.wav', 0.25, 0.5)
        exp = [np.zeros(44100 // 4)]
        exp[0] = 0.1
        self._sequence_check(ac, exp)

        # No overlap, chunk length longer than stream length.
        ac = ChoppedAudio('audio/choppedaudio_short.wav', 0.5, 0.0)
        exp = [np.zeros(44100 // 4)]
        exp[0] = 0.1
        self._sequence_check(ac, exp)

        # 50% overlap, chunk length longer than stream length.
        ac = ChoppedAudio('audio/choppedaudio_short.wav', 0.5, 0.5)
        exp = [np.zeros(44100 // 4)]
        exp[0] = 0.1
        self._sequence_check(ac, exp)

        # No overlap, five chunks fit perfectly.
        ac = ChoppedAudio('audio/choppedaudio_long.wav', 0.25, 0.0)
        exp = [
            np.zeros(44100 // 4), np.zeros(44100 // 4), np.zeros(44100 // 4),
            np.zeros(44100 // 4), np.zeros(44100 // 4)
        ]
        exp[0] = 0.1
        exp[1] = 0.1
        exp[2] = 0.2
        exp[3] = 0.2
        exp[4] = 0.4
        self._sequence_check(ac, exp)

        # No overlap, 2.5 chunks fit.
        ac = ChoppedAudio('audio/choppedaudio_long.wav', 0.5, 0.0)
        exp = [
            np.zeros(44100 // 2), np.zeros(44100 // 2), np.zeros(44100 // 4)
        ]
        exp[0] = 0.1
        exp[1] = 0.2
        exp[2] = 0.4
        self._sequence_check(ac, exp)

        # 50% overlap, 4 chunks fit perfectly.
        # 1/4-second sample values in audio: 0.1, 0.1, 0.2, 0.2, 0.4
        ac = ChoppedAudio('audio/choppedaudio_long.wav', 0.5, 0.5)
        exp = [
            np.zeros(44100 // 2), np.zeros(44100 // 2), np.zeros(44100 // 2),
            np.zeros(44100 // 2)
        ]
        exp[0] = 0.1
        exp[1][0:44100//4] = 0.1
        exp[1][44100//4:44100//2] = 0.2
        exp[2] = 0.2
        exp[3][0:44100//4] = 0.2
        exp[3][44100//4:44100//2] = 0.4
        self._sequence_check(ac, exp)

        # 1/3 overlap, 2 chunks fit perfectly.
        # 1/4-second sample values in audio: 0.1, 0.1, 0.2, 0.2, 0.4
        ac = ChoppedAudio('audio/choppedaudio_long.wav', 0.75, 1/3)
        exp = [
            np.zeros(33075), np.zeros(33075)
        ]
        exp[0][0:44100//2] = 0.1
        exp[0][44100//2:33075] = 0.2
        exp[1][0:44100//2] = 0.2
        exp[1][44100//2:33075] = 0.4
        self._sequence_check(ac, exp)

        # 2/3 overlap, 3 chunks fit perfectly.
        # 1/4-second sample values in audio: 0.1, 0.1, 0.2, 0.2, 0.4
        ac = ChoppedAudio('audio/choppedaudio_long.wav', 0.75, 2/3)
        exp = [
            np.zeros(33075), np.zeros(33075), np.zeros(33075)
        ]
        exp[0][0:44100//2] = 0.1
        exp[0][44100//2:33075] = 0.2
        exp[1][0:44100//4] = 0.1
        exp[1][44100//4:33075] = 0.2
        exp[2][0:44100//2] = 0.2
        exp[2][44100//2:33075] = 0.4
        self._sequence_check(ac, exp)

        # 50% overlap, 1.75 chunks fit perfectly.
        # 1/4-second sample values in audio: 0.1, 0.1, 0.2, 0.2, 0.4
        ac = ChoppedAudio('audio/choppedaudio_long.wav', 1.0, 0.5)
        exp = [
            np.zeros(44100), np.zeros(33075)
        ]
        exp[0][0:44100//2] = 0.1
        exp[0][44100//2:44100] = 0.2
        exp[1][0:44100//2] = 0.2
        exp[1][44100//2:33075] = 0.4
        self._sequence_check(ac, exp)

        # Do a few tests only using part of the input audio.

        # No overlap, 2 chunks fit perfectly.
        ac = ChoppedAudio(
            'audio/choppedaudio_long.wav', 0.5, 0.0, True, input_max=1.0
        )
        exp = [
            np.zeros(44100 // 2), np.zeros(44100 // 2)
        ]
        exp[0] = 0.1
        exp[1] = 0.2
        self._sequence_check(ac, exp)

        # No overlap, 2.5 chunks fit.
        ac = ChoppedAudio(
            'audio/choppedaudio_long.wav', 0.5, 0.0, True, input_max=0.75
        )
        exp = [
            np.zeros(44100 // 2), np.zeros(44100 // 4)
        ]
        exp[0] = 0.1
        exp[1] = 0.2
        self._sequence_check(ac, exp)

        # 50% overlap, 3 chunks fit perfectly.
        # 1/4-second sample values in audio: 0.1, 0.1, 0.2, 0.2, 0.4
        ac = ChoppedAudio(
            'audio/choppedaudio_long.wav', 0.5, 0.5, True, input_max=1.0
        )
        exp = [
            np.zeros(44100 // 2), np.zeros(44100 // 2), np.zeros(44100 // 2)
        ]
        exp[0] = 0.1
        exp[1][0:44100//4] = 0.1
        exp[1][44100//4:44100//2] = 0.2
        exp[2] = 0.2
        self._sequence_check(ac, exp)

