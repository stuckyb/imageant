# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from imageant.task_script.script_parser import TaskScriptParser
from imageant.task_script.script_interpreter import TaskScriptInterpreter
from imageant.task_script.script_interpreter import TaskScriptError
from imageant.task_script.task import TaskResponse


class TestTaskScriptInterpreter(unittest.TestCase):
    def _checkScript(
        self, script, responses, exp_varnames, exp_res, interp=None
    ):
        """
        Runs a script using a given list of response values and checks the
        results for each step and at the end.
        """
        if interp is None:
            tsi = TaskScriptInterpreter(script)
        else:
            tsi = interp

        cnt = 0
        task = tsi.getActiveTask()
        while task is not None:
            self.assertEqual(exp_varnames[cnt], task.varname)
            tsi.setResponse(responses[cnt])
            task = tsi.getActiveTask()
            cnt += 1

        self.assertEqual(len(exp_varnames), cnt)
        self.assertIsNone(tsi.getActiveTask())

        self.assertEqual(exp_res, tsi.getResponseVals())

    def test_stop(self):
        tsp = TaskScriptParser()

        # A script with 1 task list and multiple tasks.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
      - text: 'absent'
        action: 'keypress{a ~ N}'
      - text: 'uncertain'
        action: 'keypress{u ~ U}'

  - text: 'Whole plant present?'
    variable: 'whole_plant'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Yes}'
      - text: 'absent'
        action: 'keypress{a ~ No}'
"""     )

        tsi = TaskScriptInterpreter(script)
        tsi.setResponse(0)
        self.assertIsNotNone(tsi.getActiveTask())
        self.assertEqual({'flowers': 'Y'}, tsi.getResponseVals())

        tsi.stop()
        self.assertIsNone(tsi.getActiveTask())
        self.assertEqual({}, tsi.getResponseVals())

    def test_run(self):
        """
        Tests the correctness of script execution.
        """
        tsp = TaskScriptParser()

        # A script with no tasks.
        script = tsp.parseFromStr(r"""
# Nothing but comments.
"""     )
        tsi = TaskScriptInterpreter(script)
        self.assertIsNone(tsi.getActiveTask())

        # A script with 1 task list and 1 task.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
      - text: 'absent'
        action: 'keypress{a ~ N}'
"""     )
        # Define the responses, expected task variables, and final result.
        responses = [0]
        exp_varnames = ['flowers']
        exp_res = {'flowers': 'Y'}
        self._checkScript(script, responses, exp_varnames, exp_res)

        # A script with 1 task list and multiple tasks.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
      - text: 'absent'
        action: 'keypress{a ~ N}'
      - text: 'uncertain'
        action: 'keypress{u ~ U}'

  - text: 'Whole plant present?'
    variable: 'whole_plant'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Yes}'
      - text: 'absent'
        action: 'keypress{a ~ No}'
"""     )
        # Define the responses, expected task variables, and final result.
        responses = [2, 0]
        exp_varnames = ['flowers', 'whole_plant']
        exp_res = {'flowers': 'U', 'whole_plant': 'Yes'}
        self._checkScript(script, responses, exp_varnames, exp_res)

        # A script with 2 task lists, 3 tasks, and 1 dependency.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ N}'
      - text: 'uncertain'
        action: 'keypress{u ~ U}'

  - text: 'Whole plant present?'
    variable: 'whole_plant'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Yes}'
      - text: 'absent'
        action: 'keypress{a ~ No}'

flower_tasks:
  - text: 'Open flowers present?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ T}'
      - text: 'absent'
        action: 'keypress{a ~ F}'
      - text: 'uncertain'
        action: 'keypress{u ~ U}'
"""     )
        # Define the responses, expected task variables, and final result for
        # the scenario where we explicitly answer the dependent task.
        responses = [0, 1, 0]
        exp_varnames = ['flowers', 'open_flowers', 'whole_plant']
        exp_res = {'flowers': 'Y', 'open_flowers': 'F', 'whole_plant': 'Yes'}
        self._checkScript(script, responses, exp_varnames, exp_res)

        # Define the responses, expected task variables, and final result for
        # the scenario where we do not explicitly answer the dependent task.
        responses = [1, 0]
        exp_varnames = ['flowers', 'whole_plant']
        exp_res = {'flowers': 'N', 'open_flowers': 'N', 'whole_plant': 'Yes'}
        self._checkScript(script, responses, exp_varnames, exp_res)

        # A script with 4 task lists and 2 dependency chains, one of which is 3
        # levels deep.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ N}'
        jump-to: no_flower_tasks
      - text: 'uncertain'
        action: 'keypress{u ~ U}'

  - text: 'Whole plant present?'
    variable: 'whole_plant'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Yes}'
      - text: 'absent'
        action: 'keypress{a ~ No}'

flower_tasks:
  - text: 'Open flowers present?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ T}'
        jump-to: open_flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ F}'
      - text: 'uncertain'
        action: 'keypress{u ~ U}'

no_flower_tasks:
  - text: "There really aren't any flowers?"
    variable: 'flowers_confirm'
    default: ''
    responses:
      - text: "no, there aren't"
        action: 'keypress{n ~ N}'
      - text: 'oh, I guess there are'
        action: 'keypress{y ~ Y}'

open_flower_tasks:
  - text: 'Anthers present?'
    variable: 'anthers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ anthers_p}'
      - text: 'absent'
        action: 'keypress{a ~ anthers_a}'
      - text: 'uncertain'
        action: 'keypress{u ~ U}'

"""     )
        # Define the responses, expected task variables, and final result for
        # the scenario where we follow the long dependency chain.
        responses = [0, 0, 1, 0]
        exp_varnames = ['flowers', 'open_flowers', 'anthers', 'whole_plant']
        exp_res = {
            'flowers': 'Y', 'open_flowers': 'T', 'anthers': 'anthers_a',
            'whole_plant': 'Yes', 'flowers_confirm': ''
        }
        self._checkScript(script, responses, exp_varnames, exp_res)

        # Define the responses, expected task variables, and final result for
        # the scenario where we follow the short dependency chain.
        responses = [1, 0, 0]
        exp_varnames = ['flowers', 'flowers_confirm', 'whole_plant']
        exp_res = {
            'flowers': 'N', 'open_flowers': 'N', 'anthers': 'N',
            'whole_plant': 'Yes', 'flowers_confirm': 'N'
        }
        self._checkScript(script, responses, exp_varnames, exp_res)

        # Define the responses, expected task variables, and final result for
        # the scenario where we go halfway down the long dependency chain.
        responses = [0, 2, 0]
        exp_varnames = ['flowers', 'open_flowers', 'whole_plant']
        exp_res = {
            'flowers': 'Y', 'open_flowers': 'U', 'anthers': 'U',
            'whole_plant': 'Yes', 'flowers_confirm': ''
        }
        self._checkScript(script, responses, exp_varnames, exp_res)

        # Define the responses, expected task variables, and final result for
        # the scenario where we don't run any dependencies.
        responses = [2, 0]
        exp_varnames = ['flowers', 'whole_plant']
        exp_res = {
            'flowers': 'U', 'open_flowers': 'U', 'anthers': 'U',
            'whole_plant': 'Yes', 'flowers_confirm': ''
        }
        self._checkScript(script, responses, exp_varnames, exp_res)

        # A script with 3 task lists that visits the same dependent tasks twice
        # but does not have any cycles.  This also tests the behavior of
        # repeating tasks in a single execution.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ N}'

  - text: 'Whole plant present?'
    variable: 'whole_plant'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Yes}'
      - text: 'absent'
        action: 'keypress{a ~ No}'
        jump-to: flower_tasks

flower_tasks:
  - text: 'Open flowers present?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ T}'
        jump-to: open_flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ F}'

open_flower_tasks:
  - text: 'Anthers present?'
    variable: 'anthers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ T}'
      - text: 'absent'
        action: 'keypress{a ~ F}'

"""     )
        # Define the responses, expected task variables, and final result.
        responses = [0, 0, 1, 1, 0, 0]
        exp_varnames = [
            'flowers', 'open_flowers', 'anthers', 'whole_plant',
            'open_flowers', 'anthers'
        ]
        # The variable 'anthers' is first assigned 'F' but should end with 'T'.
        exp_res = {
            'flowers': 'Y', 'open_flowers': 'T', 'whole_plant': 'No',
            'anthers': 'T'
        }
        self._checkScript(script, responses, exp_varnames, exp_res)

    def test_script_errors(self):
        """
        Tests run-time script error detection.
        """
        tsp = TaskScriptParser()

        # A basic script.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
      - text: 'absent'
        action: 'keypress{a ~ N}'
"""     )
        # Test an invalid respons ID.
        responses = [2]
        exp_varnames = ['flowers']
        exp_res = {'flowers': 'Y'}
        with self.assertRaisesRegex(
            TaskScriptError, '2 is an invalid response ID'
        ):
            self._checkScript(script, responses, exp_varnames, exp_res)

        # A script with 1 task list and a simple reference cycle.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: main_tasks
      - text: 'absent'
        action: 'keypress{a ~ N}'
"""     )
        # Define the responses, expected task variables, and final result.
        responses = [0]
        exp_varnames = ['flowers']
        exp_res = {'flowers': 'Y'}
        with self.assertRaisesRegex(
            TaskScriptError, 'Cycle detected .* main_tasks -> main_tasks'
        ):
            self._checkScript(script, responses, exp_varnames, exp_res)

        # The same test with implicit dependency jumps.
        responses = [1]
        with self.assertRaisesRegex(
            TaskScriptError, 'Cycle detected .* main_tasks -> main_tasks'
        ):
            self._checkScript(script, responses, exp_varnames, exp_res)

        # A script with 2 task lists and a reference cycle.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ N}'

flower_tasks:
  - text: 'Open flowers present?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ T}'
        jump-to: main_tasks
      - text: 'absent'
        action: 'keypress{a ~ F}'
"""     )
        # Define the responses, expected task variables, and final result.
        responses = [0, 0]
        exp_varnames = ['flowers', 'open_flowers']
        exp_res = {'flowers': 'Y', 'open_flowers': 'T'}
        with self.assertRaisesRegex(
            TaskScriptError,
            'Cycle detected .* main_tasks -> flower_tasks -> main_tasks'
        ):
            self._checkScript(script, responses, exp_varnames, exp_res)

        # The same test with implicit dependency jumps.
        responses = [1]
        with self.assertRaisesRegex(
            TaskScriptError,
            'Cycle detected .* main_tasks -> flower_tasks -> main_tasks'
        ):
            self._checkScript(script, responses, exp_varnames, exp_res)

        # A script with 3 task lists and a reference cycle.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ N}'

flower_tasks:
  - text: 'Open flowers present?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ T}'
        jump-to: open_flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ F}'

open_flower_tasks:
  - text: 'Anthers present?'
    variable: 'anthers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ T}'
        jump-to: main_tasks
      - text: 'absent'
        action: 'keypress{a ~ F}'

"""     )
        # Define the responses, expected task variables, and final result.
        responses = [0, 0, 0]
        exp_varnames = ['flowers', 'open_flowers', 'anthers']
        exp_res = {'flowers': 'Y', 'open_flowers': 'T', 'anthers': 'T'}
        with self.assertRaisesRegex(
            TaskScriptError,
            'Cycle detected .* main_tasks -> flower_tasks -> open_flower_tasks -> main_tasks'
        ):
            self._checkScript(script, responses, exp_varnames, exp_res)

        # The same test with implicit dependency jumps.
        responses = [1]
        with self.assertRaisesRegex(
            TaskScriptError,
            'Cycle detected .* main_tasks -> flower_tasks -> open_flower_tasks -> main_tasks'
        ):
            self._checkScript(script, responses, exp_varnames, exp_res)

        # A script with 3 task lists and a tailing reference cycle.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ N}'

flower_tasks:
  - text: 'Open flowers present?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ T}'
        jump-to: open_flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ F}'

open_flower_tasks:
  - text: 'Anthers present?'
    variable: 'anthers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ T}'
        jump-to: flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ F}'

"""     )
        # Define the responses, expected task variables, and final result.
        responses = [0, 0, 0]
        exp_varnames = ['flowers', 'open_flowers', 'anthers']
        exp_res = {'flowers': 'Y', 'open_flowers': 'T', 'anthers': 'T'}
        with self.assertRaisesRegex(
            TaskScriptError,
            'Cycle detected .* main_tasks -> flower_tasks -> open_flower_tasks -> flower_tasks'
        ):
            self._checkScript(script, responses, exp_varnames, exp_res)

        # The same test with implicit dependency jumps.
        responses = [1]
        with self.assertRaisesRegex(
            TaskScriptError,
            'Cycle detected .* main_tasks -> flower_tasks -> open_flower_tasks -> flower_tasks'
        ):
            self._checkScript(script, responses, exp_varnames, exp_res)

    def test_mementos(self):
        """
        Tests the memento state save and restore system.
        """
        tsp = TaskScriptParser()

        # A script with 2 task lists, 3 tasks, and 1 dependency.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: flower_tasks
      - text: 'absent'
        action: 'keypress{a ~ N}'
      - text: 'uncertain'
        action: 'keypress{u ~ U}'

  - text: 'Whole plant present?'
    variable: 'whole_plant'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Yes}'
      - text: 'absent'
        action: 'keypress{a ~ No}'

flower_tasks:
  - text: 'Open flowers present?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{p ~ T}'
      - text: 'absent'
        action: 'keypress{a ~ F}'
      - text: 'uncertain'
        action: 'keypress{u ~ U}'
"""     )
        # Define the responses, expected task variables, and final result for
        # the scenario where we explicitly answer the dependent task.
        responses = [0, 1, 0]
        exp_varnames = ['flowers', 'open_flowers', 'whole_plant']
        exp_res = {'flowers': 'Y', 'open_flowers': 'F', 'whole_plant': 'Yes'}

        # Get the initial state, do all annotations, then restore to the
        # initial state.
        tsi = TaskScriptInterpreter(script)
        mem = tsi.getStateMemento()
        self._checkScript(script, responses, exp_varnames, exp_res, tsi)
        tsi.restoreStateFromMemento(mem)
        self._checkScript(script, responses, exp_varnames, exp_res, tsi)

        # Define the responses, expected task variables, and final result for
        # the scenario where we do not explicitly answer the dependent task.
        responses = [1, 0]
        exp_varnames = ['flowers', 'whole_plant']
        exp_res = {'flowers': 'N', 'open_flowers': 'N', 'whole_plant': 'Yes'}

        # Restore the state again, and check the new responses.
        tsi.restoreStateFromMemento(mem)
        self._checkScript(script, responses, exp_varnames, exp_res)

        # Do a step-by-step check of the mementos.
        tsi = TaskScriptInterpreter(script)
        self.assertEqual({}, tsi.getResponseVals())
        self.assertEqual('flowers', tsi.getActiveTask().varname)

        mem = tsi.getStateMemento()
        tsi.setResponse(0)
        self.assertEqual({'flowers': 'Y'}, tsi.getResponseVals())
        self.assertEqual('open_flowers', tsi.getActiveTask().varname)

        tsi.restoreStateFromMemento(mem)
        self.assertEqual({}, tsi.getResponseVals())
        self.assertEqual('flowers', tsi.getActiveTask().varname)

        tsi.setResponse(0)
        mem = tsi.getStateMemento()
        tsi.setResponse(1)
        self.assertEqual(
            {'flowers': 'Y',  'open_flowers': 'F'}, tsi.getResponseVals()
        )
        self.assertEqual('whole_plant', tsi.getActiveTask().varname)

        tsi.restoreStateFromMemento(mem)
        self.assertEqual({'flowers': 'Y'}, tsi.getResponseVals())
        self.assertEqual('open_flowers', tsi.getActiveTask().varname)

        tsi.setResponse(0)
        self.assertEqual(
            {'flowers': 'Y',  'open_flowers': 'T'}, tsi.getResponseVals()
        )
        self.assertEqual('whole_plant', tsi.getActiveTask().varname)

        mem = tsi.getStateMemento()
        tsi.setResponse(1)
        self.assertEqual(
            {'flowers': 'Y',  'open_flowers': 'T', 'whole_plant': 'No'},
            tsi.getResponseVals()
        )
        self.assertIsNone(tsi.getActiveTask())

        tsi.restoreStateFromMemento(mem)
        self.assertEqual('whole_plant', tsi.getActiveTask().varname)

        tsi.setResponse(0)
        self.assertEqual(
            {'flowers': 'Y',  'open_flowers': 'T', 'whole_plant': 'Yes'},
            tsi.getResponseVals()
        )
        self.assertIsNone(tsi.getActiveTask())

        # For the sake of completeness, test a script with no tasks.
        script = tsp.parseFromStr(r"""
# Nothing but comments.
"""     )
        tsi = TaskScriptInterpreter(script)
        mem = tsi.getStateMemento()
        self.assertIsNone(tsi.getActiveTask())
        tsi.restoreStateFromMemento(mem)
        self.assertIsNone(tsi.getActiveTask())

