# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from imageant.task_script.script_parser import TaskScriptParser
from imageant.task_script.script_parser import TaskScriptParseError
from imageant.task_script.task import TaskResponse


class TestTaskScriptParser(unittest.TestCase):
    def test_getNextActionStrToken(self):
        tsp = TaskScriptParser()

        self.assertEqual(('',''), tsp._getNextActionStrToken(''))
        self.assertEqual(('a',''), tsp._getNextActionStrToken('a'))
        self.assertEqual(('abc',''), tsp._getNextActionStrToken('abc'))
        self.assertEqual(('abc',''), tsp._getNextActionStrToken(' abc '))
        self.assertEqual(('{','a'), tsp._getNextActionStrToken('{a'))
        self.assertEqual(('}','a'), tsp._getNextActionStrToken('}a'))
        self.assertEqual(('~','a'), tsp._getNextActionStrToken('~a'))
        self.assertEqual(('abc','{'), tsp._getNextActionStrToken(' abc {'))
        self.assertEqual(
            ('abc','{ def '), tsp._getNextActionStrToken(' abc { def ')
        )
        self.assertEqual(
            ('abc def','{ '), tsp._getNextActionStrToken(' abc def { ')
        )
        self.assertEqual(
            ('abc"def','{ '), tsp._getNextActionStrToken(' abc"def { ')
        )

        # Also test strings that use quotes.
        self.assertEqual(('',''), tsp._getNextActionStrToken('""'))
        self.assertEqual(('',''), tsp._getNextActionStrToken("''"))
        self.assertEqual((' ',''), tsp._getNextActionStrToken("' '"))
        self.assertEqual(('a',''), tsp._getNextActionStrToken('"a"'))
        self.assertEqual(('abc',''), tsp._getNextActionStrToken('"abc"'))
        self.assertEqual(('"abc"',''), tsp._getNextActionStrToken('\'"abc"\''))
        self.assertEqual(('abc',' '), tsp._getNextActionStrToken(' "abc" '))
        self.assertEqual(('abc',' {'), tsp._getNextActionStrToken(' "abc" {'))
        self.assertEqual(('{a}',''), tsp._getNextActionStrToken('"{a}"'))
        self.assertEqual(
            (' abc def ',' {'), tsp._getNextActionStrToken(' " abc def " {')
        )
        self.assertEqual(
            (" abc 'def' ",' {'), tsp._getNextActionStrToken(' " abc \'def\' " {')
        )

    def test_parseActionStr(self):
        tsp = TaskScriptParser()

        self.assertEqual(
            (TaskResponse.KEYPRESS, 'p', 'Y'),
            tsp._parseActionStr('keypress{p~Y}')
        )
        self.assertEqual(
            (TaskResponse.KEYPRESS, 'p', 'Y'),
            tsp._parseActionStr(' keypress { p ~ Y } ')
        )
        self.assertEqual(
            (TaskResponse.KEYPRESS, 'p', 'Y'),
            tsp._parseActionStr('  keypress  {  p  ~  Y  }  ')
        )
        self.assertEqual(
            (TaskResponse.KEYPRESS, 'p', 'Yes'),
            tsp._parseActionStr('keypress{p~Yes}')
        )
        self.assertEqual(
            (TaskResponse.KEYPRESS, 'p', 'Yes'),
            tsp._parseActionStr('  keypress  {  p  ~  Yes  }  ')
        )
        self.assertEqual(
            (TaskResponse.KEYPRESS, 'p', 'Yes please'),
            tsp._parseActionStr('keypress{p~Yes please}')
        )
        self.assertEqual(
            (TaskResponse.KEYPRESS, 'p', 'Yes please'),
            tsp._parseActionStr('  keypress  {  p  ~  Yes please  }  ')
        )
        self.assertEqual(
            (TaskResponse.KEYPRESS, '{', 'Yes ("{~}") please'),
            tsp._parseActionStr(
                '  keypress  {  "{"  ~  \'Yes ("{~}") please\'  }  '
            )
        )

        with self.assertRaisesRegex(
            TaskScriptParseError, 'must be enclosed in braces'
        ):
            tsp._parseActionStr('keypress}p~Y}')

        with self.assertRaisesRegex(
            TaskScriptParseError, 'must be enclosed in braces'
        ):
            tsp._parseActionStr('keypress{p~Y')

        with self.assertRaisesRegex(
            TaskScriptParseError, 'Invalid key \("ab"\) for a key press'
        ):
            tsp._parseActionStr('keypress{ab~Y}')

        with self.assertRaisesRegex(
            TaskScriptParseError, 'Unexpected character\(s\)'
        ):
            tsp._parseActionStr('keypress{"a" b ~Y}')

        with self.assertRaisesRegex(
            TaskScriptParseError, 'Trailing characters after a response action'
        ):
            tsp._parseActionStr('keypress{"a" ~ Y} bcd')

        with self.assertRaisesRegex(
            TaskScriptParseError, 'Unknown response type'
        ):
            tsp._parseActionStr('invalid{"a" ~ Y} bcd')

    def _checkTaskVals(self, task, text, varname, default):
        self.assertEqual(varname, task.varname)
        self.assertEqual(text, task.text)
        if default is None:
            self.assertIsNone(task.default)
        else:
            self.assertEqual(default, task.default)

    def _checkResponseVals(
        self, response, response_id, text, key_val, response_val, jump_to,
        rtype=TaskResponse.KEYPRESS
    ):
        self.assertEqual(response_id, response.id)
        self.assertEqual(rtype, response.response_type)
        self.assertEqual(text, response.text)

        if key_val is None:
            self.assertIsNone(response.key_val)
        else:
            self.assertEqual(key_val, response.key_val)

        if response_val is None:
            self.assertIsNone(response.response_val)
        else:
            self.assertEqual(response_val, response.response_val)

        if jump_to is None:
            self.assertIsNone(response.jump_to)
        else:
            self.assertEqual(jump_to, response.jump_to)

    def test_parseFromStr(self):
        tsp = TaskScriptParser()

        script = tsp.parseFromStr('  ')
        self.assertEqual(0, script.getTaskListCount())

        script = tsp.parseFromStr(r"""
# Contains only comments...
  # and
# blank lines.
"""     )
        self.assertEqual(0, script.getTaskListCount())

        # The simplest possible non-empty script.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - action: 'keypress{p ~ Y}'
"""     )
        # Check top-level script data structure.
        self.assertEqual(1, len(script))
        self.assertTrue('main_tasks' in script)
        # Check the task list.
        t_list = script[0]
        self.assertEqual(1, len(t_list))
        task = t_list[0]
        self._checkTaskVals(task, 'Flowers present?', 'flowers', None)
        self.assertEqual(0, len(task.dependents))
        # Check the responses.
        self.assertEqual(1, len(task.responses))
        resp = task.responses[0]
        self._checkResponseVals(resp, 0, '', 'p', 'Y', None)

        # A script with all optional elements, including a dependent link.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: flowers_tasks
      - text: 'absent'
        action: 'keypress{a ~ N}'

flowers_tasks:
  - text: 'Open flowers present?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{y ~ Y}'
      - text: 'absent'
        action: 'keypress{n ~ N}'
"""     )
        # Check top-level script data structure.
        self.assertEqual(2, len(script))
        self.assertTrue('main_tasks' in script)
        self.assertTrue('flowers_tasks' in script)
        # Check task list 1.
        t_list = script[0]
        self.assertEqual(1, len(t_list))
        task = t_list[0]
        self._checkTaskVals(task, 'Flowers present?', 'flowers', None)
        self.assertEqual(['flowers_tasks'], task.dependents)
        # Check the responses.
        self.assertEqual(2, len(task.responses))
        resp = task.responses[0]
        self._checkResponseVals(resp, 0, 'present', 'p', 'Y', 'flowers_tasks')
        resp = task.responses[1]
        self._checkResponseVals(resp, 1, 'absent', 'a', 'N', None)
        # Check task list 2.
        t_list = script[1]
        self.assertEqual(1, len(t_list))
        task = t_list[0]
        self._checkTaskVals(
            task, 'Open flowers present?', 'open_flowers', 'inherit'
        )
        self.assertEqual(0, len(task.dependents))
        # Check the responses.
        self.assertEqual(2, len(task.responses))
        resp = task.responses[0]
        self._checkResponseVals(resp, 0, 'present', 'y', 'Y', None)
        resp = task.responses[1]
        self._checkResponseVals(resp, 1, 'absent', 'n', 'N', None)

        # A script with multiple dependent links.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: flowers_tasks
      - text: 'absent'
        action: 'keypress{a ~ N}'
        jump-to: absent_tasks

flowers_tasks:
  - text: 'Open flowers present?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{y ~ Y}'
      - text: 'absent'
        action: 'keypress{n ~ N}'

absent_tasks:
  - text: 'There really were no flowers?'
    variable: 'flowers_confirm'
    default: ''
    responses:
      - text: 'present'
        action: 'keypress{y ~ Y}'
      - text: 'absent'
        action: 'keypress{n ~ N}'
"""     )
        # Check top-level script data structure.
        self.assertEqual(3, len(script))
        self.assertTrue('main_tasks' in script)
        self.assertTrue('flowers_tasks' in script)
        self.assertTrue('absent_tasks' in script)
        # Check task list 1.
        t_list = script[0]
        self.assertEqual(1, len(t_list))
        task = t_list[0]
        self._checkTaskVals(task, 'Flowers present?', 'flowers', None)
        self.assertEqual(['flowers_tasks', 'absent_tasks'], task.dependents)
        # Check the responses.
        self.assertEqual(2, len(task.responses))
        resp = task.responses[0]
        self._checkResponseVals(resp, 0, 'present', 'p', 'Y', 'flowers_tasks')
        resp = task.responses[1]
        self._checkResponseVals(resp, 1, 'absent', 'a', 'N', 'absent_tasks')

        # A script with a task list with multiple tasks.
        script = tsp.parseFromStr(r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
      - text: 'absent'
        action: 'keypress{a ~ N}'

  - text: 'Whole plant present?'
    variable: 'whole_plant'
    responses:
      - text: 'yes'
        action: 'keypress{y ~ Y}'
      - text: 'no'
        action: 'keypress{n ~ N}'
"""     )
        # Check top-level script data structure.
        self.assertEqual(1, len(script))
        self.assertTrue('main_tasks' in script)
        # Check task list 1.
        t_list = script[0]
        self.assertEqual(2, len(t_list))
        # Check task 1.
        task = t_list[0]
        self._checkTaskVals(task, 'Flowers present?', 'flowers', None)
        self.assertEqual(0, len(task.dependents))
        # Check the responses.
        self.assertEqual(2, len(task.responses))
        resp = task.responses[0]
        self._checkResponseVals(resp, 0, 'present', 'p', 'Y', None)
        resp = task.responses[1]
        self._checkResponseVals(resp, 1, 'absent', 'a', 'N', None)
        # Check task 2.
        task = t_list[1]
        self._checkTaskVals(task, 'Whole plant present?', 'whole_plant', None)
        self.assertEqual(0, len(task.dependents))
        # Check the responses.
        self.assertEqual(2, len(task.responses))
        resp = task.responses[0]
        self._checkResponseVals(resp, 0, 'yes', 'y', 'Y', None)
        resp = task.responses[1]
        self._checkResponseVals(resp, 1, 'no', 'n', 'N', None)

        # A script with a task list with a variable name collision.
        script_str = r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
      - text: 'absent'
        action: 'keypress{a ~ N}'

  - text: 'Whole plant present?'
    variable: 'flowers'
    responses:
      - text: 'yes'
        action: 'keypress{y ~ Y}'
      - text: 'no'
        action: 'keypress{n ~ N}'
"""
        with self.assertRaisesRegex(
            TaskScriptParseError, 'variable names must be unique'
        ):
            tsp.parseFromStr(script_str)

        # A script with an invalid dependent link.
        script_str = r"""
main_tasks:
  - text: 'Flowers present?'
    variable: 'flowers'
    responses:
      - text: 'present'
        action: 'keypress{p ~ Y}'
        jump-to: invalid
      - text: 'absent'
        action: 'keypress{a ~ N}'

flowers_tasks:
  - text: 'Open flowers present?'
    variable: 'open_flowers'
    default: inherit
    responses:
      - text: 'present'
        action: 'keypress{y ~ Y}'
      - text: 'absent'
        action: 'keypress{n ~ N}'
"""
        with self.assertRaisesRegex(
            TaskScriptParseError, 'is not a valid .* task list name'
        ):
            tsp.parseFromStr(script_str)

