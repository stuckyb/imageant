# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest
from imageant.task_script.task_script import TaskScript


class TestTaskScript(unittest.TestCase):
    def test_list_methods(self):
        """
        Tests all of the basic TaskScript list functionality: init, adding
        lists, and retrieving lists.  Also tests overloaded operators.
        """
        ts = TaskScript()

        self.assertEqual(0, ts.getTaskListCount())
        self.assertFalse('tl_1' in ts)
        self.assertFalse('tl_2' in ts)

        tlist = ts.addTaskList('tl_1')
        tlist.append('tl_1')
        self.assertEqual(1, ts.getTaskListCount())
        self.assertEqual(1, len(ts))
        self.assertTrue('tl_1' in ts)
        self.assertFalse('tl_2' in ts)
        self.assertEqual('tl_1', ts.getTaskListName(0))

        tlist = ts.addTaskList('tl_2')
        tlist.append('tl_2')
        self.assertEqual(2, ts.getTaskListCount())
        self.assertEqual(2, len(ts))
        self.assertTrue('tl_1' in ts)
        self.assertTrue('tl_2' in ts)
        self.assertEqual('tl_1', ts.getTaskListName(0))
        self.assertEqual('tl_2', ts.getTaskListName(1))

        self.assertEqual('tl_1', ts[0][0])
        self.assertEqual('tl_1', ts['tl_1'][0])

        self.assertEqual('tl_2', ts[1][0])
        self.assertEqual('tl_2', ts['tl_2'][0])

        with self.assertRaisesRegex(
            IndexError, 'Invalid type for accessing task lists'
        ):
            ts[1.02]

        with self.assertRaisesRegex(IndexError, 'Invalid task list index'):
            ts[2]

        with self.assertRaisesRegex(IndexError, 'Invalid task list index'):
            ts.getTaskListName(2)

        with self.assertRaisesRegex(KeyError, 'Invalid task list name'):
            ts['tl_3']

        with self.assertRaisesRegex(
            ValueError, 'a task list with the name "tl_2" already exists'
        ):
            ts.addTaskList('tl_2')

