# Copyright (C) 2020 Brian J. Stucky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest
from imageant.task_undo_stack import TaskUndoStack


class TestTaskUndoStack(unittest.TestCase):
    def test_hasRewindableLastObject(self):
        stack = TaskUndoStack()

        self.assertFalse(stack.hasRewindableLastObject(0))
        self.assertFalse(stack.hasRewindableLastObject(1))

        stack.push(0, '0_state0')
        self.assertFalse(stack.hasRewindableLastObject(0))
        self.assertTrue(stack.hasRewindableLastObject(1))

        stack.push(0, '0_state1')
        self.assertFalse(stack.hasRewindableLastObject(0))
        self.assertTrue(stack.hasRewindableLastObject(1))

        stack.push(1, '1_state0')
        self.assertTrue(stack.hasRewindableLastObject(1))
        self.assertTrue(stack.hasRewindableLastObject(2))

    def test_rewindLastMediaObject(self):
        stack = TaskUndoStack()

        self.assertEqual((None, None), stack.rewindLastMediaObject(1))

        stack.push(0, '0_state0')
        self.assertEqual((None, None), stack.rewindLastMediaObject(0))

        stack.clear()
        stack.push(0, '0_state0')
        stack.push(0, '0_state1')
        self.assertEqual((None, None), stack.rewindLastMediaObject(0))

        stack.clear()
        stack.push(0, '0_state0')
        self.assertEqual((0, '0_state0'), stack.rewindLastMediaObject(1))

        stack.clear()
        stack.push(0, '0_state0')
        stack.push(0, '0_state1')
        self.assertEqual((None, None), stack.rewindLastMediaObject(0))
        self.assertEqual((0, '0_state0'), stack.rewindLastMediaObject(1))

        stack.clear()
        stack.push(0, '0_state0')
        stack.push(0, '0_state1')
        stack.push(1, '1_state0')
        self.assertEqual((0, '0_state0'), stack.rewindLastMediaObject(1))

        stack.clear()
        stack.push(0, '0_state0')
        stack.push(0, '0_state1')
        stack.push(1, '1_state0')
        stack.push(1, '1_state1')
        self.assertEqual((0, '0_state0'), stack.rewindLastMediaObject(1))

        stack.clear()
        stack.push(0, '0_state0')
        stack.push(0, '0_state1')
        stack.push(1, '1_state0')
        stack.push(1, '1_state1')
        stack.push(1, '1_state2')
        self.assertEqual((1, '1_state0'), stack.rewindLastMediaObject(2))

        stack.clear()
        stack.push(0, '0_state0')
        stack.push(0, '0_state1')
        stack.push(1, '1_state0')
        self.assertEqual((1, '1_state0'), stack.rewindLastMediaObject(2))

        stack.clear()
        stack.push(0, '0_state0')
        stack.push(0, '0_state1')
        stack.push(1, '1_state0')
        stack.push(2, '2_state0')
        self.assertEqual((1, '1_state0'), stack.rewindLastMediaObject(2))

        stack.clear()
        stack.push(0, '0_state0')
        stack.push(0, '0_state1')
        stack.push(1, '1_state0')
        stack.push(1, '1_state1')
        stack.push(1, '1_state2')
        stack.push(2, '2_state0')
        self.assertEqual((1, '1_state0'), stack.rewindLastMediaObject(2))

        stack.clear()
        stack.push(0, '0_state0')
        stack.push(0, '0_state1')
        stack.push(1, '1_state0')
        stack.push(1, '1_state1')
        stack.push(1, '1_state2')
        stack.push(2, '2_state0')
        stack.push(2, '2_state1')
        stack.push(2, '2_state2')
        self.assertEqual((1, '1_state0'), stack.rewindLastMediaObject(2))

